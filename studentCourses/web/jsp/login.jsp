<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 28.03.2015
  Time: 8:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<form class="editable-form login-page-form" name="loginForm" method="POST" action="controller">
    <input type="hidden" name="command" value="login"/>
    <h3><fmt:message key="register.login"/></h3>
    <input type="text" name="login" size="50" pattern="^[а-яА-Яa-zA-Z0-9''-'\s]{1,40}$"
           autocomplete="on" autofocus required/>
    
    <div class="error-message"><c:out value="${errorLoginMessage}"></c:out></div>
    <h3><fmt:message key="register.password"/></h3>
    <input type="password" name="password" size="50"
           pattern="(?!^[0-9]*$)(?!^[а-яА-Яa-zA-Z]*$)^([а-яА-Яa-zA-Z0-9]{8,10})$" required/>

    <div class="error-message"><c:out value="${errorPassMessage}"></c:out></div>

    <div class="error-message"><c:out value="${errorLoginPassMessage}"></c:out></div>
    
    ${nullPage}
    
    <input type="submit" value="<fmt:message key="button.submit"/>"/>
</form>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>