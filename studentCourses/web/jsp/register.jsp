<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 12.04.2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<form class="editable-form register-form" name="registerForm" method="POST" action="controller">
    <input type="hidden" name="command" value="register"/>

    <h2><fmt:message key="register.form"/></h2>

    <h3><fmt:message key="register.login"/></h3>
        <input type="text" name="login" size="50" pattern="^[а-яА-Яa-zA-Z0-9''-'\s]{1,40}$" autofocus
               required><div class="error-message">${errorLoginExistMessage}</div>
    <div class="error-message">${errorLoginMessage}</div>
    <h3><fmt:message key="register.password"/></h3>
        <input type="password" name="password" size="50"
               pattern="(?!^[0-9]*$)(?!^[а-яА-Яa-zA-Z]*$)^([а-яА-Яa-zA-Z0-9]{8,10})$" required>
    <div class="error-message">${errorPassMessage}</div>
    <h3><fmt:message key="register.name"/></h3>
        <input type="text" name="name" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    <div class="error-message">${errorNameMessage}</div>
    <h3><fmt:message key="register.lastName"/></h3>
        <input type="text" name="lastname" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    <div class="error-message">${errorSurnameMessage}</div>
    <h3><fmt:message key="register.university"/></h3>
        <input type="text" name="university" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    <div class="error-message">${errorUniversityMessage}</div>
    <h3><fmt:message key="register.faculty"/></h3>
        <input type="text" name="faculty" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    <div class="error-message">${errorFacultyMessage}</div>
    <h3><fmt:message key="register.course"/></h3>
        <input type="text" name="course" size="10" pattern="^\d{1}$" required>
    <div class="error-message">${errorCourseMessage}</div>
    <h3><fmt:message key="register.group"/></h3>
        <input type="text" name="group" size="10" pattern="^\d{1,3}$" required>
    <div class="error-message">${errorGroupMessage}</div>
    <input type="submit" value="<fmt:message key="button.submit"/>"/>
</form>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>