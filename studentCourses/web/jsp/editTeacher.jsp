<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 07.05.2015
  Time: 4:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
${inBlackList}

<form class="editable-form" name="editForm" method="POST" action="controller">
    <input type="hidden" name="command" value="editTeacher"/>

    <h2><fmt:message key="edit.form"/></h2>

    <h3><fmt:message key="register.name"/></h3>
        <input type="text" name="name" value="${teacher.name}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorNameMessage}</div>
    <h3><fmt:message key="register.lastName"/></h3>
        <input type="text" name="lastname" value="${teacher.lastname}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorSurnameMessage}</div>
    <h3><fmt:message key="register.university"/></h3>
        <input type="text" name="university" value="${teacher.university}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorUniversityMessage}</div>
    <h3><fmt:message key="register.position"/></h3>
        <input type="text" name="position" value="${teacher.position}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorPositionMessage}</div>
    <input type="submit" value="<fmt:message key="button.submit"/>"/>

</form>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>

