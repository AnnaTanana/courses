function updateInfo(data) {
    $.ajax({
        type: "POST",
        url: "controller",
        async: false,
        data: "command=" + $(data).val(),
        success: function () {
            location.reload();
        }});
}

function changeMark(studentLogin, mark, courseId) {
    $.ajax({
        type: "POST",
        url: "controller",
        async: false,
        data: "command=changeMarkAndStatus&login=" + studentLogin +"&value=" + $(mark).val() + "&id="+ courseId + "&type=mark"
});
}

function changeStatus(studentLogin, status, courseId) {
    $.ajax({
        type: "POST",
        url: "controller",
        async: false,
        data: "command=changeMarkAndStatus&login=" + studentLogin + "&value=" + $(status).val() + "&id="+ courseId + "&type=status"
    });
    if ('attending' == $(status).val()) {
        $(status).parent().parent().find("td input.customInput").removeAttr('disabled');
    }
    else {
        $(status).parent().parent().find("td input.customInput").attr('disabled','disabled');
    }
}