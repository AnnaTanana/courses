function updateInfo(data) {
    $.ajax({
        type: "POST",
        url: "controller",
        async: false,
        data: "command=" + $(data).val(),
        success: function () {
            location.reload();
        }});
}

function changeMark(studentLogin, mark, courseId) {
    if ((mark.value < 0) || (mark.value>10) || !($.isNumeric(mark.value))){
        if(mark.parentElement.childNodes[2].innerHTML == '') {
            mark.parentElement.getElementsByClassName('error')[0].innerHTML = "<div>Incorrect input value</div>";
            return;
        }
    }
    mark.parentElement.getElementsByClassName('error')[0].innerHTML = '';
    $.ajax({
        type: "POST",
        url: "controller",
        async: false,
        data: "command=changeMarkAndStatus&login=" + studentLogin +"&value=" + mark.value + "&id="+ courseId + "&type=mark"

});
}

function changeStatus(studentLogin, status, courseId) {
    $.ajax({
        type: "POST",
        url: "controller",
        async: false,
        data: "command=changeMarkAndStatus&login=" + studentLogin + "&value=" + $(status).val() + "&id="+ courseId + "&type=status"
    });
    if ('attending' == $(status).val()) {
        $(status).parent().parent().find("td input.customInput").removeAttr('disabled');
    }
    else {
        $(status).parent().parent().find("td input.customInput").attr('disabled','disabled');
    }
}