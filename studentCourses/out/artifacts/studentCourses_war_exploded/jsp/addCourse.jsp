<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 03.05.2015
  Time: 16:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<form class="editable-form" name="addCourseForm" method="POST" action="controller">
    <input type="hidden" name="command" value="addCourse"/>

    <h3><fmt:message key="register.name"/></h3>
        <input type="text" name="name" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    <div class="error-message">${errorNameMessage}</div>
    <h3><fmt:message key="course.description"/></h3>
        <textarea name="description" ROWS=3 COLS=38 required="required"></textarea>
    <div class="error-message">${errorDescriptionMessage}</div>
        <input type="submit" value="<fmt:message key="button.submit"/>"/>
</form>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>

