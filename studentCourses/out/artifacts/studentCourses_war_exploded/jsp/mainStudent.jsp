<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 28.03.2015
  Time: 8:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="profile-page">
<p>${inBlackList}</p>
<c:if test="${ checkBlackList eq false}">
    <ctg:role-info role="student" login="${login}">

                    <h2><fmt:message key="main.hello"/>, ${pageContext.session.getAttribute("login")}!</h2>

                    <form method="POST" action="controller">
                        <input type="hidden" name="page" value="editStudent">
                        <button class="edit-button" name="command" value="reference"><fmt:message key="button.editInfo"/></button>
                    </form>

    </ctg:role-info>
<div class="description-part">
    <h2><fmt:message key="main.personal.info"/></h2>
    <form name="infoForm" method="POST" action="controller">
        <table>
            <tr>
                <td><h3><fmt:message key="register.name"/></h3></td>
                <td><h4><c:out value="${ student.name }"/></h4></td>
            </tr>
            <tr>
                <td><h3><fmt:message key="register.lastName"/></h3></td>
                <td><h4><c:out value="${ student.lastname }"/></h4></td>
            </tr>
            <tr>
                <td><h3><fmt:message key="register.university"/></h3></td>
                <td><h4><c:out value="${ student.university }"/></h4></td>
            </tr>
            <tr>
                <td><h3><fmt:message key="register.faculty"/></h3></td>
                <td><h4><c:out value="${ student.faculty }"/></h4></td>
            </tr>
            <tr>
                <td><h3><fmt:message key="register.course"/></h3></td>
                <td><h4><c:out value="${ student.course }"/></h4></td>
            </tr>
            <tr>
                <td><h3><fmt:message key="register.group"/></h3></td>
                <td><h4><c:out value="${ student.group }"/></h4></td>
            </tr>
        </table>
    </form>
</div>

    <h3><fmt:message key="main.courses"/></h3>
    <p>${coursesNotChosen}</p>
    <div class="table-part">
    <table>
        <c:forEach var="elem" items="${courses.entrySet()}" varStatus="status">
            <tr>
                <form name="coursesForm" method="POST" action="controller">
                    <input type="hidden" name="id" value="${elem.key.key}"/>
                    <td>
                        <button name="command" value="showCourse">${ elem.key.value}</button>
                    </td>
                    <c:choose>
                        <c:when test="${ (elem.value.key != 0) && !(elem.value.value.toString() eq 'DELETED') }">
                            <td class="mark"><c:out value="${ elem.value.key}"/></td>
                        </c:when>
                        <c:otherwise>
                            <td></td>
                        </c:otherwise>
                    </c:choose>
                    <td><c:set var="status" value="${elem.value.value.toString()}"/>
                        <c:choose>
                            <c:when test="${ status eq 'ATTENDING' }">
                                <fmt:message key='student.attending'/>
                            </c:when>
                            <c:when test="${ status eq 'PASSED'  }">
                                <fmt:message key="student.passed"/>
                            </c:when>
                            <c:when test="${ status eq 'FAILED'  }">
                                <fmt:message key='student.failed'/>
                            </c:when>
                            <c:when test="${ status eq 'CANCELLED'  }">
                                <fmt:message key='student.cancelled'/>
                            </c:when>
                            <c:when test="${ status eq 'DELETED'  }">
                                <fmt:message key='student.deleted'/>
                            </c:when>
                        </c:choose>
                    </td>
                    <td>
                    <c:if test="${ (login eq sessionScope.login) && ('ATTENDING' eq (elem.value.value.toString())) }">

                            <button class="delete-button" name="command" value="cancelCourse"><fmt:message key="button.cancel"/></button>

                    </c:if>
                    </td>
                </form>
            </tr>
        </c:forEach>
    </table>
        </div>

    <ctg:role-info role="student" login="${login}">
        <form name="chooseCoursesForm" method="POST" action="controller">
            <button class="link-button" name="command" value="showCourses"><fmt:message key="button.choose.course"/></button>
        </form>
    </ctg:role-info>
    <p>
    <ctg:role-info role="teacher">
        <form method="POST" action="controller">
            <input type="hidden" name="login" value="${login}"/>
            <button class="link-button" name="command" value="ban"><fmt:message key="button.ban"/></button>
        </form>
    </ctg:role-info>
    </p>
</c:if>
</div>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
