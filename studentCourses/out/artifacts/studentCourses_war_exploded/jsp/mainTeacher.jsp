<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 20.04.2015
  Time: 8:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="profile-page">
<ctg:role-info login="${login}">
    <h2><fmt:message key="main.hello"/>, ${pageContext.session.getAttribute("login")}!</h2>
    <form method="POST" action="controller">
        <input type="hidden" name="page" value="editTeacher">
    <button class="edit-button" name="command" value="reference"><fmt:message key="button.editInfo"/></button>
    </form>
</ctg:role-info>
    <div class="description-part">
<h2><fmt:message key="main.personal.info"/></h2>
<form name="infoForm" method="POST" action="controller">
    <table>
        <tr>
            <td><h3><fmt:message key="register.name"/></h3></td>
            <td><h4><c:out value="${ teacher.name }"/></h4></td>
        </tr>
        <tr>
            <td><h3><fmt:message key="register.lastName"/></h3></td>
            <td><h4><c:out value="${ teacher.lastname }"/></h4></td>
        </tr>
        <tr>
            <td><h3><fmt:message key="register.university"/></h3></td>
            <td><h4><c:out value="${ teacher.university }"/></h4></td>
        </tr>
        <tr>
            <td><h3><fmt:message key="register.position"/></h3></td>
            <td><h4><c:out value="${ teacher.position }"/></h4></td>
        </tr>
    </table>
</form>
    </div>

<h3><fmt:message key="main.courses"/></h3>
    <p>${coursesNotChosen}</p>
<div class="table-part">
<table>
    <c:forEach var="elem" items="${courses.entrySet()}" varStatus="status">
        <tr>
            <form name="coursesForm" method="POST" action="controller">
                <td>
                    <button name="command" value="showCourse">${ elem.value}</button>
                </td>
                <input type="hidden" name="id" value="${elem.key}"/>
                <ctg:role-info role="teacher" login="${login}">
                <td>
                    <button class="delete-button" name="command" value="deleteCourse"><fmt:message key="button.delete.course"/></button>
                </td>
                </ctg:role-info>
            </form>
        </tr>
    </c:forEach>
</table>
</div>
<ctg:role-info login="${login}" role="teacher">
    <form name="showCoursesForm" method="POST" action="controller">
        <button class="link-button" name="command" value="showCourses"><fmt:message key="button.show.courses"/></button>
    </form>
    <form name="showStudentsForm" method="POST" action="controller">
        <button class="link-button" name="command" value="showStudents"><fmt:message key="button.show.students"/></button>
    </form>
    <form name="showBlackListForm" method="POST" action="controller">
        <button class="link-button" name="command" value="showBlackList"><fmt:message key="button.blackList"/></button>
    </form>
    <form name="addCourseForm" method="POST" action="controller">
        <input type="hidden" name="page" value="addCourse">
        <button class="link-button" name="command" value="reference"><fmt:message key="button.add.course"/></button>
    </form>
</ctg:role-info>
</div>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
