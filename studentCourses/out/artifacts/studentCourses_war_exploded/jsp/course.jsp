<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 22.04.2015
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="course-page">
    <p>${courseDeleted}</p>
    <c:if test="${isDeleted eq false}">
    <h2><c:out value="${courseName}"></c:out></h2>
    <c:if test="${(sessionScope.role eq 'teacher') && (teacherLogin eq sessionScope.login)}">
        <form method="POST" action="controller">
            <input type="hidden" name="courseId" value="${courseId}">
            <input type="hidden" name="page" value="editCourse">
            <button class="edit-button" name="command" value="reference"><fmt:message key="button.edit"/></button>
        </form>
    </c:if>
    <div class="description-part">
        <div class="row"><h3><fmt:message key="course.teacher"/></h3>

            <form name="teacherForm" method="POST" action="controller">
                <input type="hidden" name="login" value="${teacher.login}"/>

                <button  class="name-button" name="command" value="showTeacher">${teacher.name} ${teacher.lastname}</button>

            </form>
        </div>
        <h3><fmt:message key="course.description"/></h3>
        <p>${description}</p>
    </div>

    <h3><fmt:message key="course.students"/></h3>
    <p>${nobodyTakeCourse}</p>
    <table class="student-table">
        <c:forEach var="elem" items="${students.entrySet()}" varStatus="status">
            <tr>
                <form name="studentsForm" method="POST" action="controller">
                    <td><input type="hidden" name="login" value="${elem.key.login}"/></td>
                    <td>
                        <button name="command"
                                value="showStudent">${elem.key.name} ${elem.key.lastname}</button>
                    </td>
                </form>
                <form name="marksAndStatus" method="POST" action="controller" id="data">
                    <ctg:role-info role="teacher" login="${teacherLogin}">
                        <td>${wrongMark}
                            <input class="customInput" <c:if test="${!('ATTENDING' eq (elem.value.value.toString()))}">disabled="disabled" </c:if> name="mark" value="${elem.value.key}"
                                    onchange="changeMark( '${elem.key.login}', this, '${courseId}' )"><span class="error error-message"></span></td>
                        <td><select name="status" onchange="changeStatus( '${elem.key.login}', this, '${courseId}' )">
                            <option <c:if test="${elem.value.value.toString() eq 'ATTENDING'}">selected</c:if> value="attending"><fmt:message key="student.attending"/></option>
                            <option <c:if test="${elem.value.value.toString() eq 'PASSED'}">selected</c:if> value="passed"><fmt:message key="student.passed"/></option>
                            <option <c:if test="${elem.value.value.toString() eq 'FAILED'}">selected</c:if> value="failed"><fmt:message key="student.failed"/></option>
                        </select></td>
                        </ctg:role-info>
                </form>
            </tr>
        </c:forEach>
    </table>


    <h3><fmt:message key="course.reviews"/></h3>
    <p>${noReviewForCourse}</p>
    <table class="review-table">
        <c:forEach var="elem" items="${reviews}" varStatus="status">
            <tr>
                <form name="reviewsForm" method="POST" action="controller">
                    <input type="hidden" name="login" value="${elem.student.login}"/>
                    <input type="hidden" name="reviewId" value="${elem.reviewId}">
                    <input type="hidden" name="courseId" value="${courseId}">
                    <td class="review"><p>${elem.review}</p></td>
                    <td class="review-name">
                        <button class="name-button" name="command" value="showStudent">${elem.student.name} ${elem.student.lastname}</button>

                    <c:if test="${(elem.student.login eq sessionScope.login) || ((sessionScope.role eq 'teacher') && (teacherLogin eq sessionScope.login))}">

                            <button class="delete-button" name="command" value="deleteReview"><fmt:message
                                    key="button.delete.review"/></button>
                        </td>
                    </c:if>
                </form>
            </tr>
        </c:forEach>
    </table>
    <c:if test="${(sessionScope.role eq 'student') && (isAttendingCourse)}">
        <form name="commentsForm" method="POST" action="controller">
            <input type="hidden" name="command" value="leaveReview"/>
            <td><input type="hidden" name="courseId" value="${courseId}"></td>
        <textarea name="review" placeholder="<fmt:message key="course.leave.comment"/>" ROWS=3 COLS=30
                  required="required"></textarea>
            <input type="submit" value="<fmt:message key="button.submit"/>"/>
        </form>
    </c:if>
    </c:if>
</div>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
