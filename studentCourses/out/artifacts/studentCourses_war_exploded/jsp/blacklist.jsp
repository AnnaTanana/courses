<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 02.05.2015
  Time: 16:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-page">
<h3>${emptyBlacklist}</h3>
<table>
    <c:forEach var="elem" items="${students}" varStatus="status">
        <tr>
            <form name="blackListForm" method="POST" action="controller">
                <input type="hidden" name="login" value="${elem.login}"/>
                <td class="name-cell">${elem.name} ${elem.lastname}</td>
                <td>
                    <button name="command" value="removeFromBlacklist"><fmt:message
                            key="button.removeFromBlacklist"/></button>
                </td>
            </form>
        </tr>
    </c:forEach>
</table>
    </div>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
