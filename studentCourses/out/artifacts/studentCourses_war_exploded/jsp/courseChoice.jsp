<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 28.04.2015
  Time: 14:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div class="table-page">
    <h3>${noCoursesToChoose}</h3>
<table>
    <c:forEach var="elem" items="${moreCourses.entrySet()}" varStatus="status">
        <tr>
            <form method="POST" action="controller">
                <input type="hidden" name="id" value="${elem.key}"/>
                <td>
                    <button name="command" value="showCourse">${elem.value.name}</button>
                </td>
            </form>
            <form method="POST" action="controller">
                <input type="hidden" name="login" value="${elem.value.teacher.login}"/>
                <td>
                    <button name="command"
                            value="showTeacher">${elem.value.teacher.name} ${elem.value.teacher.lastname}</button>
                </td>
            </form>
            <ctg:role-info role="student">
                <form method="POST" action="controller">
                    <input type="hidden" name="id" value="${elem.key}"/>
                    <td>
                        <button name="command" value="takeCourse"><fmt:message key="button.register.course"/></button>
                    </td>
                </form>
            </ctg:role-info>
        </tr>
    </c:forEach>
</table>
</div>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
