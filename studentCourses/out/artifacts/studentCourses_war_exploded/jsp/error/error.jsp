<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 28.03.2015
  Time: 8:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<body>
Request from ${pageContext.errorData.requestURI} is failed
<br/>
Servlet name or type: ${pageContext.errorData.servletName}
<br/>
Status code: ${pageContext.errorData.statusCode}
<br/>
Exception: ${pageContext.errorData.throwable}
<br/>
<a href="controller?"><fmt:message key="href.toMain"/></a>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
