<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 02.05.2015
  Time: 5:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<div class="table-page student-list">
<h3>${studNotFound}</h3>
<table>
    <c:forEach var="elem" items="${students}" varStatus="status">
        <tr>
            <form name="coursesForm" method="POST" action="controller">
                <input type="hidden" name="login" value="${elem.login}"/>
                <td>
                    <button name="command" value="showStudent">${elem.name} ${elem.lastname}</button>
                </td>
            </form>
        </tr>
    </c:forEach>
</table>
    </div>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>
