<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 02.04.2015
  Time: 4:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="ru_RU" scope="session" />
<fmt:bundle basename="resource.pagecontent">
    <html><head>
        <title><fmt:message key="label.title" /></title>
    </head>
    <footer>
    <fmt:message key="footer.copyright" />
    </footer>
</html>
</fmt:bundle>
