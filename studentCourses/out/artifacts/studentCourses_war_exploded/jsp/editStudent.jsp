<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 05.05.2015
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
${inBlackList}


<form class="editable-form" name="editForm" method="POST" action="controller">
    <input type="hidden" name="command" value="editStudent"/>

    <h2><fmt:message key="edit.form"/></h2>
    
    <h3><fmt:message key="register.name"/></h3>
        <input type="text" name="name" value="${student.name}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorNameMessage}</div>
    <h3><fmt:message key="register.lastName"/></h3>
        <input type="text" name="lastname" value="${student.lastname}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorSurnameMessage}</div>
    <h3><fmt:message key="register.university"/></h3>
        <input type="text" name="university" value="${student.university}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorUniversityMessage}</div>
    <h3><fmt:message key="register.faculty"/></h3>
        <input type="text" name="faculty" value="${student.faculty}" size="50" pattern="^[а-яА-Яa-zA-Z''-'\s]{1,40}$" required>
    
    <div class="error-message">${errorFacultyMessage}</div>
    <h3><fmt:message key="register.course"/></h3>
        <input type="text" name="course" value="${student.course}" size="10" pattern="^\d{1}$" required>
    
    <div class="error-message">${errorCourseMessage}</div>
    <h3><fmt:message key="register.group"/></h3>
        <input type="text" name="group" value="${student.group}" size="10" pattern="^\d{1,3}$" required>
    
    <div class="error-message">${errorGroupMessage}</div>
    <input type="submit" value="<fmt:message key="button.submit"/>"/>

</form>
</body>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>

