<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 28.03.2015
  Time: 8:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ include file="WEB-INF/jspf/header.jspf" %>
<body>
<fmt:setBundle basename="resources.pagecontent"/>
<div>
    <div class="login-register">
        <form method="POST" action="controller">
            <input type="hidden" name="page" value="login">
            <button name="command" value="reference"><fmt:message key="button.login"/></button>
            </form>
        <form method="POST" action="controller">
            <input type="hidden" name="page" value="register">
            <button name="command" value="reference"><fmt:message key="button.register"/></button>
        </form>
    </div>
    <div class="content">
        <h3><fmt:message key="index.welcome"/></h3>
    </div>
</div>
</body>
<%@ include file="WEB-INF/jspf/footer.jspf" %>
