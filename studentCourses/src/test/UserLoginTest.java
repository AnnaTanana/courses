package test;

import by.tanana.courses.logic.Validator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by user on 14.05.2015.
 */
public class UserLoginTest {
    @Test
    public void userLoginTest(){
        String login = "f?!";
        boolean isLoginValid = Validator.isLoginValid(login);
        Assert.assertEquals(false, isLoginValid);
    }
}
