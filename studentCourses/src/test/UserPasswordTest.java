package test;

import by.tanana.courses.logic.Validator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by user on 14.05.2015.
 */
public class UserPasswordTest {
    @Test
    public void userPasswordTest() {
        String password = "5786";
        boolean isPasswordValid = Validator.isPasswordValid(password);
        Assert.assertEquals(false, isPasswordValid);
    }
}
