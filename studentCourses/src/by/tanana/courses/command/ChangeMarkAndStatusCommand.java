package by.tanana.courses.command;

import by.tanana.courses.entity.Course;
import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.User;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.CourseLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 05.05.2015.
 */
public class ChangeMarkAndStatusCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";

    private static final String PARAM_NAME_MARK = "mark";
    private static final String PARAM_NAME_STATUS = "status";
    private static final String PARAM_NAME_TYPE = "type";
    private static final String PARAM_NAME_COURSE_ID = "id";
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_VALUE = "value";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";
    private static final String ATTRIBUTE_WRONG_MARK = "wrongMark";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        String role = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (login == null || !(ATTRIBUTE_NAME_TEACHER.equals(role))) {
            return ConfigurationManager.getProperty("path.page.index");
        }

        String type = request.getParameter(PARAM_NAME_TYPE);
        int courseId = Integer.parseInt(request.getParameter(PARAM_NAME_COURSE_ID));
        String studentLogin = request.getParameter(PARAM_NAME_LOGIN);
        try {
            if (PARAM_NAME_MARK.equals(type)) {
                int mark = Integer.parseInt(request.getParameter(PARAM_NAME_VALUE));
                if (!UserLogic.checkMark(mark)) {
                    request.setAttribute(ATTRIBUTE_WRONG_MARK, messageManager.getProperty("message.wrongMark"));
                } else {
                    UserLogic.changeMark(studentLogin, mark, courseId);
                }

            } else if (PARAM_NAME_STATUS.equals(type)) {
                Status status = Status.valueOf(request.getParameter(PARAM_NAME_VALUE).toUpperCase());
                UserLogic.changeStatus(studentLogin, status, courseId);
            }

            return null;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
