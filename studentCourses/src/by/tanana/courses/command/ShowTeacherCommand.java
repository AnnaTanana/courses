package by.tanana.courses.command;

import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.Teacher;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 23.04.2015.
 */
public class ShowTeacherCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";
    private static final String ATTRIBUTE_NAME_COURSES = "courses";
    private static final String ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE = "coursesNotChosen";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String loginFromSession = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        if (loginFromSession == null) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        String login = request.getParameter(PARAM_NAME_LOGIN);

        request.setAttribute(ATTRIBUTE_NAME_LOGIN, login);
        try {
            Teacher teacher = UserLogic.getTeacherInfo(login);
            request.setAttribute(ATTRIBUTE_NAME_TEACHER, teacher);


            HashMap<Integer, String> courses = UserLogic.getTeacherCourses(login);
            if (!courses.isEmpty()) {
                request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
            } else {
                request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
            }

            String page = ConfigurationManager.getProperty("path.page.main.teacher");
            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
