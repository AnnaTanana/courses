package by.tanana.courses.command;

import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 28.03.2015.
 */
public class LogoutCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        if (login == null) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        String page = ConfigurationManager.getProperty("path.page.index");
        // уничтожение сессии
        request.getSession().invalidate();
        return page;
    }
}
