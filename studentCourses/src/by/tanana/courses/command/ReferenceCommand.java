package by.tanana.courses.command;

import by.tanana.courses.entity.Course;
import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.Teacher;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.CourseLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 08.05.2015.
 */
public class ReferenceCommand implements ActionCommand {
    private static String PARAM_NAME_PAGE = "page";
    private static String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String PARAM_NAME_COURSE_ID = "courseId";
    private static final String ATTRIBUTE_NAME_COURSE = "course";
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_IN_BLACKLIST = "inBlackList";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String page = request.getParameter(PARAM_NAME_PAGE);
        try {
            switch (page) {
                case "addCourse": {
                    String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
                    if (login == null) {
                        return ConfigurationManager.getProperty("path.page.index");
                    }
                    return ConfigurationManager.getProperty("path.page.add.course");
                }
                case "editCourse": {
                    String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
                    if (login == null) {
                        return ConfigurationManager.getProperty("path.page.index");
                    }
                    int courseId = Integer.parseInt(request.getParameter(PARAM_NAME_COURSE_ID));
                    request.setAttribute(PARAM_NAME_COURSE_ID, courseId);

                    Course course = CourseLogic.getCourse(courseId);
                    request.setAttribute(ATTRIBUTE_NAME_COURSE, course);

                    return ConfigurationManager.getProperty("path.page.edit.course");
                }
                case "editStudent": {
                    String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
                    if (login == null) {
                        return ConfigurationManager.getProperty("path.page.index");
                    }

                    if (UserLogic.isInBlackList(login)) {
                        if (login.equals(request.getSession().getAttribute(PARAM_NAME_LOGIN))) {
                            request.setAttribute(ATTRIBUTE_IN_BLACKLIST, messageManager.getProperty("blacklist.me"));
                        } else {
                            request.setAttribute(ATTRIBUTE_IN_BLACKLIST, messageManager.getProperty("blacklist.user"));
                        }
                        return ConfigurationManager.getProperty("path.page.index");
                    }

                    Student student = UserLogic.getStudentInfo(login);
                    request.setAttribute(ATTRIBUTE_NAME_STUDENT, student);

                    return ConfigurationManager.getProperty("path.page.edit.student");

                }
                case "editTeacher": {
                    String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
                    if (login == null) {
                        return ConfigurationManager.getProperty("path.page.index");
                    }
                    Teacher teacher = UserLogic.getTeacherInfo(login);
                    request.setAttribute(ATTRIBUTE_NAME_TEACHER, teacher);

                    return ConfigurationManager.getProperty("path.page.edit.teacher");
                }
                case "login":
                    return ConfigurationManager.getProperty("path.page.login");
                case "register":
                    return ConfigurationManager.getProperty("path.page.register");
                default:
                    return ConfigurationManager.getProperty("path.page.index");
            }
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
