package by.tanana.courses.command;

import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.Teacher;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.LoginLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 28.03.2015.
 */
public class LoginCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_NAME_COURSES = "courses";
    private static final String PARAM_NAME_STUDENT= "student";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_NAME_LOGIN_OR_PASSWORD_ERROR = "errorLoginPassMessage";
    private static final String ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE = "coursesNotChosen";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";
    private static final String ATTRIBUTE_IN_BLACKLIST = "inBlackList";
    private static final String ATTRIBUTE_CHECK_BLACKLIST = "checkBlackList";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String page;

        if (LoginLogic.checkLoginFields(request) == false) {
            return ConfigurationManager.getProperty("path.page.login");
        }
        try {
            if (LoginLogic.checkLogin(request)) {
                request.getSession().setAttribute(ATTRIBUTE_NAME_LOGIN, login);
                request.getSession().setAttribute(SESSION_ATTRIBUTE_ROLE, LoginLogic.defineRoll(request));
                request.setAttribute(ATTRIBUTE_NAME_LOGIN, login);
                if (PARAM_NAME_STUDENT.equals(request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE).toString())) {
                    boolean isInBlackList = false;
                    if (UserLogic.isInBlackList(login)) {
                        request.setAttribute(ATTRIBUTE_IN_BLACKLIST, messageManager.getProperty("blacklist.me"));
                        isInBlackList = true;
                    } else {
                        Student student = UserLogic.getStudentInfo(login);
                        request.setAttribute(ATTRIBUTE_NAME_STUDENT, student);
                        HashMap<Pair<Integer, String>, Pair<Integer, Status>> courses = UserLogic.getStudentsCourses(login);
                        if (!courses.isEmpty()) {
                            request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
                        } else {
                            request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
                        }
                    }
                    request.setAttribute(ATTRIBUTE_CHECK_BLACKLIST, isInBlackList);
                    page = ConfigurationManager.getProperty("path.page.main.student");

                } else {
                    Teacher teacher = UserLogic.getTeacherInfo(login);
                    request.setAttribute(ATTRIBUTE_NAME_TEACHER, teacher);

                    HashMap<Integer, String> courses = UserLogic.getTeacherCourses(login);
                    if (!courses.isEmpty()) {
                        request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
                    } else {
                        request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
                    }

                    page = ConfigurationManager.getProperty("path.page.main.teacher");
                }
            } else {
                request.setAttribute(ATTRIBUTE_NAME_LOGIN_OR_PASSWORD_ERROR, new MessageManager(request).getProperty("message.loginAndPassError"));
                page = ConfigurationManager.getProperty("path.page.login");
            }

            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
