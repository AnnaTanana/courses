package by.tanana.courses.command;

import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 28.03.2015.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request) throws CommandException;
}
