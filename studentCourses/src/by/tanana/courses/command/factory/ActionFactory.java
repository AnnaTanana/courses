package by.tanana.courses.command.factory;

import by.tanana.courses.command.ActionCommand;
import by.tanana.courses.command.EmptyCommand;
import by.tanana.courses.command.client.CommandEnum;
import by.tanana.courses.resource.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 28.03.2015.
 */
public class ActionFactory {
    private static final Logger LOG = Logger.getLogger(ActionFactory.class);

    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current;
    // извлечение имени команды из запроса
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
    // если команда не задана в текущем запросе
            return new EmptyCommand();
        }
    // получение объекта, соответствующего команде
        try {
            CommandEnum currentEnum =
                    CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
            return current;
        } catch (IllegalArgumentException e) {
            LOG.error(e);
        }
        return new EmptyCommand();
    }
}
