package by.tanana.courses.command;

import by.tanana.courses.entity.Course;
import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.CourseLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 06.05.2015.
 */
public class EditCourseCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_COURSE_NAME = "courseName";
    private static final String ATTRIBUTE_NAME_DESCRIPTION = "description";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";
    private static final String ATTRIBUTE_NAME_STUDENTS = "students";
    private static final String ATTRIBUTE_NAME_REVIEWS = "reviews";
    private static final String ATTRIBUTE_NOBODY_TAKE_COURSE = "nobodyTakeCourse";
    private static final String ATTRIBUTE_NO_REVIEW_FOR_COURSE = "noReviewForCourse";
    private static final String ATTRIBUTE_NAME_COURSE_ID = "courseId";
    private static final String ATTRIBUTE_IS_ATTENDING_COURSE = "isAttendingCourse";
    private static final String ATTRIBUTE_NAME_TEACHER_LOGIN = "teacherLogin";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_NAME_IS_DELETED = "isDeleted";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        String role = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (login == null || !(ATTRIBUTE_NAME_TEACHER.equals(role))) {
            return ConfigurationManager.getProperty("path.page.index");
        }

        if (CourseLogic.checkCourseFields(request) == false) {
            return ConfigurationManager.getProperty("path.page.edit.course");
        }
        // создание пользователя
        try {
            CourseLogic.updateCourse(request);

            int id = Integer.parseInt(request.getParameter(ATTRIBUTE_NAME_COURSE_ID));
            request.setAttribute(ATTRIBUTE_NAME_COURSE_ID, id);

            Course course = CourseLogic.getCourse(id);
            request.setAttribute(ATTRIBUTE_NAME_TEACHER_LOGIN, course.getTeacher().getLogin());
            request.setAttribute(ATTRIBUTE_NAME_COURSE_NAME, course.getName());
            request.setAttribute(ATTRIBUTE_NAME_DESCRIPTION, course.getDescription());
            request.setAttribute(ATTRIBUTE_NAME_TEACHER, course.getTeacher());
            HashMap<Student, Pair<Integer, Status>> marksForCourse = CourseLogic.getMarksForCourse(id);
            if (!marksForCourse.isEmpty()) {
                request.setAttribute(ATTRIBUTE_NAME_STUDENTS, marksForCourse);
            } else {
                request.setAttribute(ATTRIBUTE_NOBODY_TAKE_COURSE, messageManager.getProperty("message.nobodyTakeCourse"));
            }
            if (!course.getReviews().isEmpty()) {
                request.setAttribute(ATTRIBUTE_NAME_REVIEWS, course.getReviews());
            } else {
                request.setAttribute(ATTRIBUTE_NO_REVIEW_FOR_COURSE, messageManager.getProperty("message.noReviews"));
            }

            boolean isAttendingCourse = UserLogic.isAttendingCourse(login, id);
            request.setAttribute(ATTRIBUTE_IS_ATTENDING_COURSE, isAttendingCourse);
            boolean isDeleted = false;
            request.setAttribute(ATTRIBUTE_NAME_IS_DELETED, isDeleted);
            String page = ConfigurationManager.getProperty("path.page.course");
            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
