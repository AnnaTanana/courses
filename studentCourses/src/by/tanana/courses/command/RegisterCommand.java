package by.tanana.courses.command;

import by.tanana.courses.dao.StudentDAO;
import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.LoginLogic;
import by.tanana.courses.logic.RegisterLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 12.04.2015.
 */
public class RegisterCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_ROLL = "role";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_NAME_COURSES = "courses";
    private static final String PARAM_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE = "coursesNotChosen";
    private static final String ATTRIBUTE_CHECK_BLACKLIST = "checkBlackList";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String page;

        if (RegisterLogic.checkRegistrationFields(request) == false) {
            return ConfigurationManager.getProperty("path.page.register");
        }
        // создание пользователя
        try {
            if (RegisterLogic.checkLogin(request) == false) {
                page = ConfigurationManager.getProperty("path.page.register");
            } else {
                RegisterLogic.createUser(request);
                request.getSession().setAttribute(ATTRIBUTE_NAME_LOGIN, login);
                request.getSession().setAttribute(ATTRIBUTE_NAME_ROLL, PARAM_NAME_STUDENT);

                boolean isInBlackList = false;


                request.setAttribute(ATTRIBUTE_NAME_LOGIN, login);

                Student student = UserLogic.getStudentInfo(login);
                request.setAttribute(ATTRIBUTE_NAME_STUDENT, student);

                HashMap<Pair<Integer, String>, Pair<Integer, Status>> courses = UserLogic.getStudentsCourses(login);
                if (!courses.isEmpty()) {
                    request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
                } else {
                    request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
                }
                request.setAttribute(ATTRIBUTE_CHECK_BLACKLIST, isInBlackList);
                // определение пути к mainStudent.jsp
                page = ConfigurationManager.getProperty("path.page.main.student");
            }
            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
