package by.tanana.courses.command;

import by.tanana.courses.entity.Teacher;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.CourseLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 03.05.2015.
 */
public class AddCourseCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";
    private static final String ATTRIBUTE_NAME_COURSES = "courses";
    private static final String ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE = "coursesNotChosen";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String loginFromSession = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        String role = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (loginFromSession == null || !(ATTRIBUTE_NAME_TEACHER.equals(role))) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        MessageManager messageManager = new MessageManager(request);
        if (CourseLogic.checkCourseFields(request) == false) {
            return ConfigurationManager.getProperty("path.page.add.course");
        } else {
            try {
                CourseLogic.addCourse(request);


                String login = request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN).toString();

                request.setAttribute(ATTRIBUTE_NAME_LOGIN, login);

                Teacher teacher = UserLogic.getTeacherInfo(login);
                request.setAttribute(ATTRIBUTE_NAME_TEACHER, teacher);


                HashMap<Integer, String> courses = UserLogic.getTeacherCourses(login);
                if (!courses.isEmpty()) {
                    request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
                } else {
                    request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
                }

                String page = ConfigurationManager.getProperty("path.page.main.teacher");
                return page;
            } catch (LogicException e) {
                throw new CommandException(e);
            }
        }
    }
}
