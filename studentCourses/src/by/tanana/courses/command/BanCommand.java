package by.tanana.courses.command;

import by.tanana.courses.dao.StudentDAO;
import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 02.05.2015.
 */
public class BanCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_NAME_COURSES = "courses";
    private static final String ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE = "coursesNotChosen";
    private static final String ATTRIBUTE_IN_BLACKLIST = "inBlackList";
    private static final String ATTRIBUTE_CHECK_BLACKLIST = "checkBlackList";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String loginFromSession = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        String role = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (loginFromSession == null || !(ATTRIBUTE_NAME_TEACHER.equals(role))) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        String login = request.getParameter(PARAM_NAME_LOGIN);
        try {
            UserLogic.ban(login);

            boolean isInBlackList = false;

            if (UserLogic.isInBlackList(login)) {
                if (login.equals(request.getSession().getAttribute(PARAM_NAME_LOGIN))) {
                    request.setAttribute(ATTRIBUTE_IN_BLACKLIST, messageManager.getProperty("blacklist.me"));
                } else {
                    request.setAttribute(ATTRIBUTE_IN_BLACKLIST, messageManager.getProperty("blacklist.user"));
                }
                isInBlackList = true;

            } else {
                request.setAttribute(ATTRIBUTE_NAME_LOGIN, login);

                Student student = UserLogic.getStudentInfo(login);
                request.setAttribute(ATTRIBUTE_NAME_STUDENT, student);

                HashMap<Pair<Integer, String>, Pair<Integer, Status>> courses = UserLogic.getStudentsCourses(login);
                if (!courses.isEmpty()) {
                    request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
                } else {
                    request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
                }

            }
            request.setAttribute(ATTRIBUTE_CHECK_BLACKLIST, isInBlackList);
            return ConfigurationManager.getProperty("path.page.main.student");
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
