package by.tanana.courses.command;

import by.tanana.courses.entity.Course;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.CourseLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 29.04.2015.
 */
public class TakeCourseCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_MORE_COURSES = "moreCourses";
    private static final String PARAM_NAME_ID = "id";
    private static final String ATTRIBUTE_NO_COURSES_TO_CHOOSE = "noCoursesToChoose";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        try {
            String login = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
            String role = (String) request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
            if (login == null || !(ATTRIBUTE_NAME_STUDENT.equals(role)) || UserLogic.isInBlackList(login)) {
                return ConfigurationManager.getProperty("path.page.index");
            }

            int courseId = Integer.parseInt(request.getParameter(PARAM_NAME_ID));

            UserLogic.takeCourse(login, courseId);

            HashMap<Integer, Course> moreCourses = CourseLogic.getMoreCourses(login);
            if (!moreCourses.isEmpty()) {
                request.setAttribute(ATTRIBUTE_NAME_MORE_COURSES, moreCourses);
            } else {
                request.setAttribute(ATTRIBUTE_NO_COURSES_TO_CHOOSE, messageManager.getProperty("message.noCoursesToChoose"));
            }
            String page = ConfigurationManager.getProperty("path.page.course.choice");
            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
