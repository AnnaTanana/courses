package by.tanana.courses.command.client;

import by.tanana.courses.command.*;

/**
 * Created by user on 28.03.2015.
 */
public enum CommandEnum {
    SHOWCOURSES {
        {
            this.command = new ShowCoursesCommand();
        }
    },
    REFERENCE {
        {
            this.command = new ReferenceCommand();
        }
    },
    EDITCOURSE {
        {
            this.command = new EditCourseCommand();
        }
    },
    EDITTEACHER {
        {
            this.command = new EditTeacherCommand();
        }
    },
    EDITSTUDENT{
        {
            this.command = new EditStudentCommand();
        }
    },
    CHANGEMARKANDSTATUS {
        {
            this.command = new ChangeMarkAndStatusCommand();
        }
    },
    DELETEREVIEW {
        {
            this.command = new DeleteReviewCommand();
        }
    },
    LEAVEREVIEW {
        {
            this.command = new LeaveReviewCommand();
        }
    },
    DELETECOURSE {
        {
            this.command = new DeleteCourseCommand();
        }
    },
    ADDCOURSE {
        {
          this.command = new AddCourseCommand();
        }
    },
    REMOVEFROMBLACKLIST {
        {
            this.command = new RemoveFromBlacklistCommand();
        }

    },
    SHOWBLACKLIST {
        {
            this.command = new ShowBlackListCommand();
        }
    },
    BAN {
        {
            this.command = new BanCommand();
        }
    },
    SHOWSTUDENTS {
        {
            this.command = new ShowStudentsCommand();
        }
    },
    TAKECOURSE {
        {
            this.command = new TakeCourseCommand();
        }
    },
    CHOOSECOURSE {
        {
            this.command = new ShowCoursesCommand();
        }
    },
    CANCELCOURSE {
        {
            this.command = new CancelCourseCommand();
        }

    },
    SHOWTEACHER {
        {
            this.command = new ShowTeacherCommand();
        }
    },
    SHOWSTUDENT{
        {
            this.command = new ShowStudentCommand();
        }
    },
    SHOWCOURSE {
        {
            this.command = new ShowCourseCommand();
        }
    },
    EN {
        {
            this.command = new ChangeLanguageCommand("en_us");
        }
    },
    RU {
        {
            this.command = new ChangeLanguageCommand("ru_ru");
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    };
    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}
