package by.tanana.courses.command;

import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 18.04.2015.
 */
public class ChangeLanguageCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LANGUAGE = "language";

    private String language;

    public ChangeLanguageCommand(String language) {
        this.language = language;
    }

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(ATTRIBUTE_NAME_LANGUAGE, language);
        request.setAttribute(ATTRIBUTE_NAME_LANGUAGE, language);
        return null;
    }
}
