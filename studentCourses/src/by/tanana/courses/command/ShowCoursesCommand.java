package by.tanana.courses.command;

import by.tanana.courses.entity.Course;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.CourseLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 27.04.2015.
 */
public class ShowCoursesCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_NAME_MORE_COURSES = "moreCourses";
    private static final String ATTRIBUTE_NO_COURSES_TO_CHOOSE = "noCoursesToChoose";
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String login = (String)request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        if (login == null) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        request.setAttribute(ATTRIBUTE_NAME_STUDENT, ATTRIBUTE_NAME_STUDENT);
        try {
            if (ATTRIBUTE_NAME_STUDENT.equals(request.getSession().getAttribute("role").toString())) {
                HashMap<Integer, Course> moreCourses = CourseLogic.getMoreCourses(login);
                if (!moreCourses.isEmpty()) {
                    request.setAttribute(ATTRIBUTE_NAME_MORE_COURSES, moreCourses);
                } else {
                    request.setAttribute(ATTRIBUTE_NO_COURSES_TO_CHOOSE, messageManager.getProperty("message.noCoursesToChoose"));
                }
            } else {
                HashMap<Integer, Course> moreCourses = CourseLogic.getAllCourses();
                if (!moreCourses.isEmpty()) {
                    request.setAttribute(ATTRIBUTE_NAME_MORE_COURSES, moreCourses);
                } else {
                    request.setAttribute(ATTRIBUTE_NO_COURSES_TO_CHOOSE, messageManager.getProperty("message.noCoursesToChoose"));
                }

            }

            String page = ConfigurationManager.getProperty("path.page.course.choice");
            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
