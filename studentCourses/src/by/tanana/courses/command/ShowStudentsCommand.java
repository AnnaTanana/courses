package by.tanana.courses.command;

import by.tanana.courses.entity.Student;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Created by user on 02.05.2015.
 */
public class ShowStudentsCommand implements ActionCommand {
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_STUDENTS = "students";
    private static final String ATTRIBUTE_STUDENTS_NOT_FOUND_MESSAGE = "studNotFound";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_NAME_TEACHER = "teacher";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        MessageManager messageManager = new MessageManager(request);
        String loginFromSession = (String) request.getSession().getAttribute(ATTRIBUTE_NAME_LOGIN);
        String role = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (loginFromSession == null || !(ATTRIBUTE_NAME_TEACHER.equals(role))) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        try {
            ArrayList<Student> students = UserLogic.getAllStudents();
            if (!students.isEmpty()) {
                request.setAttribute(ATTRIBUTE_NAME_STUDENTS, students);
            } else {
                request.setAttribute(ATTRIBUTE_STUDENTS_NOT_FOUND_MESSAGE, messageManager.getProperty("message.studNotFound"));
            }
            return ConfigurationManager.getProperty("path.page.show.students");
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
