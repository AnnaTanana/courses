package by.tanana.courses.command;

import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.logic.RegisterLogic;
import by.tanana.courses.logic.UserLogic;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by user on 05.05.2015.
 */
public class EditStudentCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_LOGIN = "login";
    private static final String ATTRIBUTE_NAME_STUDENT = "student";
    private static final String ATTRIBUTE_NAME_COURSES = "courses";
    private static final String ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE = "coursesNotChosen";
    private static final String ATTRIBUTE_CHECK_BLACKLIST = "checkBlackList";
    private static final String SESSION_ATTRIBUTE_ROLE = "role";

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String login = (String) request.getSession().getAttribute(PARAM_NAME_LOGIN);
        String role = (String)request.getSession().getAttribute(SESSION_ATTRIBUTE_ROLE);
        if (login == null || !(ATTRIBUTE_NAME_STUDENT.equals(role))) {
            return ConfigurationManager.getProperty("path.page.index");
        }
        request.setAttribute(ATTRIBUTE_NAME_LOGIN, login);
        MessageManager messageManager = new MessageManager(request);
        String page;

        if (UserLogic.checkStudentFields(request) == false) {
            return ConfigurationManager.getProperty("path.page.edit.student");
        }
        // создание пользователя
        try {
            UserLogic.updateStudent(request);

            boolean isInBlackList = false;

            Student student = UserLogic.getStudentInfo(login);
            request.setAttribute(ATTRIBUTE_NAME_STUDENT, student);

            HashMap<Pair<Integer, String>, Pair<Integer, Status>> courses = UserLogic.getStudentsCourses(login);
            if (!courses.isEmpty()) {
                request.setAttribute(ATTRIBUTE_NAME_COURSES, courses);
            } else {
                request.setAttribute(ATTRIBUTE_COURSES_NOT_CHOSEN_MESSAGE, messageManager.getProperty("message.coursesNotChose"));
            }
            request.setAttribute(ATTRIBUTE_CHECK_BLACKLIST, isInBlackList);
            // определение пути к mainStudent.jsp
            page = ConfigurationManager.getProperty("path.page.main.student");

            return page;
        } catch (LogicException e) {
            throw new CommandException(e);
        }
    }
}
