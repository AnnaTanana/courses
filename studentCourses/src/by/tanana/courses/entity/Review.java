package by.tanana.courses.entity;

/**
 * Created by user on 20.04.2015.
 */
public class Review {
    private String review;
    private Student student;
    private int reviewId;

    public Review() {
    }

    public Review(String review, Student student, int reviewId) {
        this.review = review;
        this.student = student;
        this.reviewId = reviewId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
