package by.tanana.courses.entity;

/**
 * Created by user on 04.05.2015.
 */
public enum Status {
    ATTENDING, CANCELLED, PASSED, FAILED, DELETED
}
