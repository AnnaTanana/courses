package by.tanana.courses.entity;

import java.util.ArrayList;

/**
 * Created by user on 20.04.2015.
 */
public class Teacher extends User{
    private String name;
    private String lastname;
    private String university;
    private String position;

    public Teacher() {
    }

    public Teacher(String login, String password, String name, String lastname, String university, String position) {
        super(login, password);
        this.name = name;
        this.lastname = lastname;
        this.university = university;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
