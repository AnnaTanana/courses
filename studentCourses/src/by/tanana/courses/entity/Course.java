package by.tanana.courses.entity;

import java.util.ArrayList;

/**
 * Created by user on 20.04.2015.
 */
public class Course {
    private String name;
    private Teacher teacher;
    private ArrayList<Review> reviews;
    private ArrayList<Student> students;
    private String description;
    private Boolean hidden;

    public Course() {
    }

    public Course(String name, Teacher teacher, ArrayList<Review> reviews, ArrayList<Student> students, String description, Boolean hidden) {
        this.name = name;
        this.teacher = teacher;
        this.reviews = reviews;
        this.students = students;
        this.description = description;
        this.hidden = hidden;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public ArrayList<Review> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        this.reviews = reviews;
    }
}
