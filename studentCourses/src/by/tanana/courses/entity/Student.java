package by.tanana.courses.entity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 29.03.2015.
 */
public class Student extends User{
    private String name;
    private String lastname;
    private String university;
    private String faculty;
    private int course;
    private int group;

    public Student() {
    }

    public Student(String login, String password, String name, String lastname, String university, String faculty, int course, int group) {
        super(login, password);
        this.name = name;
        this.lastname = lastname;
        this.university = university;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

}
