package by.tanana.courses.controller;

import by.tanana.courses.command.ActionCommand;
import by.tanana.courses.command.factory.ActionFactory;
import by.tanana.courses.dao.ConnectionPool;
import by.tanana.courses.exception.CommandException;
import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.exception.PoolException;
import by.tanana.courses.resource.ConfigurationManager;
import by.tanana.courses.resource.MessageManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by user on 28.03.2015.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(Controller.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void init() throws ServletException
    {
        String fileName = getServletContext().getRealPath("/config/log4j.xml");
        new DOMConfigurator().doConfigure(fileName, LogManager.getLoggerRepository());
        try {
            ConnectionPool.getInstance().init();
        } catch (PoolException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = null;
        // определение команды, пришедшей из JSP
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);
        /*
        * вызов реализованного метода execute() и передача параметров
        * классу-обработчику конкретной команды
        */
        try {
            page = command.execute(request);
        // метод возвращает страницу ответа
            if (page != null) {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
                // вызов страницы ответа на запрос
                dispatcher.forward(request, response);
            }
        } catch (CommandException e) {
            LOG.error(e);
            getServletContext().getRequestDispatcher("path.page.error").forward(request, response);
        }
    }

    @Override
    public void destroy()
    {
        ConnectionPool.getInstance().closePool();
    }
}
