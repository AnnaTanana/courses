package by.tanana.courses.logic;

import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 14.05.2015.
 */
public class Validator {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LASTNAME = "lastname";
    private static final String PARAM_NAME_UNIVERSITY = "university";
    private static final String PARAM_NAME_FACULTY = "faculty";
    private static final String PARAM_NAME_COURSE = "course";
    private static final String PARAM_NAME_GROUP = "group";
    private static final String PARAM_NAME_POSITION = "position";
    private static final String PARAM_NAME_DESCRIPTION = "description";

    private static final String REGEX_FOR_LOGIN = "^[а-яА-Яa-zA-Z0-9''-'\\s]{1,40}$";
    private static final String REGEX_FOR_PASSWORD = "(?!^[0-9]*$)(?!^[а-яА-Яa-zA-Z]*$)^([а-яА-Яa-zA-Z0-9]{8,10})$";
    private static final String REGEX_FOR_NAME = "^[а-яА-Яa-zA-Z''-'\\s]{1,40}$";
    private static final String REGEX_FOR_SURNAME = "^[а-яА-Яa-zA-Z''-'\\s]{1,40}$";
    private static final String REGEX_FOR_UNIVERSITY = "^[а-яА-Яa-zA-Z''-'\\s]{1,40}$";
    private static final String REGEX_FOR_FACULTY = "^[а-яА-Яa-zA-Z''-'\\s]{1,40}$";
    private static final String REGEX_FOR_COURSE = "^\\d{1}$";
    private static final String REGEX_FOR_GROUP = "^\\d{1,3}$";
    private static final String REGEX_FOR_POSITION = "^[а-яА-Яa-zA-Z''-'\\s]{1,40}$";

    private static final String ATTRIBUTE_NAME_ERROR_LOGIN_MESSAGE = "errorLoginMessage";
    private static final String ATTRIBUTE_NAME_ERROR_PASSWORD_MESSAGE = "errorPassMessage";
    private static final String ATTRIBUTE_NAME_ERROR_NAME_MESSAGE = "errorNameMessage";
    private static final String ATTRIBUTE_NAME_ERROR_SURNAME_MESSAGE = "errorSurnameMessage";
    private static final String ATTRIBUTE_NAME_ERROR_UNIVERSITY_MESSAGE = "errorUniversityMessage";
    private static final String ATTRIBUTE_NAME_ERROR_FACULTY_MESSAGE = "errorFacultyMessage";
    private static final String ATTRIBUTE_NAME_ERROR_COURSE_MESSAGE = "errorCourseMessage";
    private static final String ATTRIBUTE_NAME_ERROR_GROUP_MESSAGE = "errorGroupMessage";
    private static final String ATTRIBUTE_NAME_ERROR_POSITION_MESSAGE = "errorPositionMessage";
    private static final String ATTRIBUTE_NAME_ERROR_DESCRIPTION_MESSAGE = "errorDescriptionMessage";

    public static boolean isLoginValid(String login) {
        if (!login.matches(REGEX_FOR_LOGIN) || login == null || login.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean isPasswordValid(String password) {
        if (!password.matches(REGEX_FOR_PASSWORD) || password == null || password.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean checkLogin(HttpServletRequest request, MessageManager messageManager) {
        String login = request.getParameter(PARAM_NAME_LOGIN);
        login = login.trim();
        if (!Validator.isLoginValid(login)) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_LOGIN_MESSAGE, messageManager.getProperty("message.loginError"));
            return false;
        }
        return true;
    }

    public static boolean checkPassword(HttpServletRequest request, MessageManager messageManager) {
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        password = password.trim();
        if (!Validator.isPasswordValid(password)) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_PASSWORD_MESSAGE, messageManager.getProperty("message.passwordError"));
            return false;
        }
        return true;
    }

    public static boolean checkName(HttpServletRequest request, MessageManager messageManager) {
        String name = request.getParameter(PARAM_NAME_NAME);
        name = name.trim();
        if (!name.matches(REGEX_FOR_NAME) || name == null || name.isEmpty()) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_NAME_MESSAGE, messageManager.getProperty("message.nameError"));
            return false;
        }
        return true;
    }

    public static boolean checkSurname(HttpServletRequest request, MessageManager messageManager) {
        String surname = request.getParameter(PARAM_NAME_LASTNAME);
        surname = surname.trim();
        if (!surname.matches(REGEX_FOR_SURNAME) || surname == null) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_SURNAME_MESSAGE, messageManager.getProperty("message.surnameError"));
            return false;
        }
        return true;
    }

    public static boolean checkUniversity(HttpServletRequest request, MessageManager messageManager) {
        String university = request.getParameter(PARAM_NAME_UNIVERSITY);
        university = university.trim();
        if (!university.matches(REGEX_FOR_UNIVERSITY) || university == null || university.isEmpty()) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_UNIVERSITY_MESSAGE, messageManager.getProperty("message.universityError"));
            return false;
        }
        return true;
    }

    public static boolean checkFaculty(HttpServletRequest request, MessageManager messageManager) {
        String faculty = request.getParameter(PARAM_NAME_FACULTY);
        faculty = faculty.trim();
        if (!faculty.matches(REGEX_FOR_FACULTY) || faculty == null || faculty.isEmpty()) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_FACULTY_MESSAGE, messageManager.getProperty("message.facultyError"));
            return false;
        }
        return true;
    }

    public static boolean checkCourse(HttpServletRequest request, MessageManager messageManager) {
        String course = request.getParameter(PARAM_NAME_COURSE);
        course = course.trim();
        if (!course.matches(REGEX_FOR_COURSE) || course == null || course.isEmpty()) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_COURSE_MESSAGE, messageManager.getProperty("message.courseError"));
            return false;
        }
        return true;
    }

    public static boolean checkGroup(HttpServletRequest request, MessageManager messageManager) {
        String group = request.getParameter(PARAM_NAME_GROUP);
        group = group.trim();
        if (!group.matches(REGEX_FOR_GROUP) || group == null || group.isEmpty()){
            request.setAttribute(ATTRIBUTE_NAME_ERROR_GROUP_MESSAGE, messageManager.getProperty("message.groupError"));
            return false;
        }
        return true;
    }

    public static boolean checkPosition(HttpServletRequest request, MessageManager messageManager) {
        String position = request.getParameter(PARAM_NAME_POSITION);
        position = position.trim();
        if (!position.matches(REGEX_FOR_POSITION) || position == null || position.isEmpty()) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_POSITION_MESSAGE, messageManager.getProperty("message.positionError"));
            return false;
        }
        return true;
    }

    public static boolean checkDescription(HttpServletRequest request, MessageManager messageManager) {
        String description = request.getParameter(PARAM_NAME_DESCRIPTION);
        description = description.trim();
        if (description.length() > 500  || description == null || description.isEmpty()) {
            request.setAttribute(ATTRIBUTE_NAME_ERROR_DESCRIPTION_MESSAGE, messageManager.getProperty("message.descriptionError"));
            return false;
        }
        return true;
    }
}
