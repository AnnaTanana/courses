package by.tanana.courses.logic;

import by.tanana.courses.dao.StudentDAO;
import by.tanana.courses.dao.TeacherDAO;
import by.tanana.courses.dao.UserDAO;
import by.tanana.courses.entity.Review;
import by.tanana.courses.entity.Status;
import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.Teacher;
import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 21.04.2015.
 */
public class UserLogic {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_REVIEW = "review";
    private static final String PARAM_NAME_COURSE_ID = "courseId";

    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LASTNAME = "lastname";
    private static final String PARAM_NAME_UNIVERSITY = "university";
    private static final String PARAM_NAME_FACULTY = "faculty";
    private static final String PARAM_NAME_COURSE = "course";
    private static final String PARAM_NAME_GROUP = "group";
    private static final String PARAM_NAME_POSITION = "position";

    public static HashMap<Pair<Integer, String>, Pair<Integer, Status>> getStudentsCourses(String login) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            return studentDAO.getCourses(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void cancelStudentCourse(String login, int courseId) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.cancelCourse(login, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static Student getStudentInfo(String login) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            return studentDAO.getStudentInfo(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void takeCourse(String login, int courseId) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.takeCourse(login, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static Teacher getTeacherInfo(String login) throws LogicException {
        TeacherDAO teacherDAO = new TeacherDAO();
        try {
            return teacherDAO.getTeacherInfo(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static HashMap<Integer, String> getTeacherCourses(String login) throws LogicException {
        TeacherDAO teacherDAO = new TeacherDAO();
        try {
            return teacherDAO.getCourses(login);
        } catch (DAOException e) {
            throw new LogicException();
        }
    }

    public static ArrayList<Student> getAllStudents() throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            return studentDAO.getAllStudents();
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void ban(String login) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.ban(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean isInBlackList(String login) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            return studentDAO.isInBlackList(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void deleteFromBlackList(String login) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.deleteFromBlackList(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static ArrayList<Student> showBlackList() throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            return studentDAO.showBlackList();
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void addReview(HttpServletRequest request) throws LogicException {
        Review review = new Review();
        Student student = new Student();
        student.setLogin(request.getSession().getAttribute(PARAM_NAME_LOGIN).toString());
        review.setStudent(student);
        String reviewText = request.getParameter(PARAM_NAME_REVIEW);
        reviewText = reviewText.replaceAll("<", "&lt");
        reviewText = reviewText.replaceAll(">", "&gt");
        review.setReview(reviewText);
        int courseId = Integer.parseInt(request.getParameter(PARAM_NAME_COURSE_ID));
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.addReview(review, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }

    }

    public static void deleteReview(int reviewId) throws LogicException {
        UserDAO userDAO = new UserDAO();
        try {
            userDAO.deleteReview(reviewId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean isAttendingCourse(String login, int courseId) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            return studentDAO.isAttendingCourse(login, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean checkStudentFields(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager(request);

        boolean check = true;
        if (!Validator.checkName(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkSurname(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkUniversity(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkFaculty(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkCourse(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkGroup(request, messageManager)){
            check = false;
        }
        return check;
    }

    public static void updateStudent(HttpServletRequest request) throws LogicException {
        Student student = new Student();
        student.setLogin(request.getAttribute(PARAM_NAME_LOGIN).toString());
        student.setName(request.getParameter(PARAM_NAME_NAME));
        student.setLastname(request.getParameter(PARAM_NAME_LASTNAME));
        student.setUniversity(request.getParameter(PARAM_NAME_UNIVERSITY));
        student.setFaculty(request.getParameter(PARAM_NAME_FACULTY));
        student.setCourse(Integer.parseInt(request.getParameter(PARAM_NAME_COURSE)));
        student.setGroup(Integer.parseInt(request.getParameter(PARAM_NAME_GROUP)));

        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.updateStudent(student);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void changeMark(String login, int mark, int courseId) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.changeMark(login, mark, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void changeStatus(String login, Status status, int courseId) throws LogicException {
        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.changeStatus(login, status, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean checkTeacherFields(HttpServletRequest request) {
        MessageManager messageManager = new MessageManager(request);

        String position = request.getParameter(PARAM_NAME_POSITION);

        boolean check = true;
        if (!Validator.checkName(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkSurname(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkUniversity(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkPosition(request, messageManager)) {
            check = false;
        }
        return check;
    }

    public static void updateTeacher(HttpServletRequest request) throws LogicException {
        Teacher teacher = new Teacher();
        teacher.setLogin(request.getAttribute(PARAM_NAME_LOGIN).toString());
        teacher.setName(request.getParameter(PARAM_NAME_NAME));
        teacher.setLastname(request.getParameter(PARAM_NAME_LASTNAME));
        teacher.setUniversity(request.getParameter(PARAM_NAME_UNIVERSITY));
        teacher.setPosition(request.getParameter(PARAM_NAME_POSITION));

        TeacherDAO teacherDAO = new TeacherDAO();
        try {
            teacherDAO.updateTeacher(teacher);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean checkMark(int mark) {
        return ((mark < 10) && (mark > 0));
    }
}
