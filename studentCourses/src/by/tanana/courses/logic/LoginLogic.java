package by.tanana.courses.logic;

import by.tanana.courses.dao.StudentDAO;
import by.tanana.courses.dao.UserDAO;
import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 28.03.2015.
 */
public class LoginLogic {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    private static final String STUDENT = "student";
    private static final String TEACHER = "teacher";

    public static boolean checkLoginFields (HttpServletRequest request) {
        MessageManager messageManager = new MessageManager(request);

        boolean check = true;

        if (!Validator.checkLogin(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkPassword(request, messageManager)) {
            check = false;
        }
        return check;
    }

    public static String defineRoll(HttpServletRequest request) throws LogicException {
        String login = request.getParameter(PARAM_NAME_LOGIN);

        StudentDAO studentDAO = new StudentDAO();
        try {
            if (studentDAO.contains(login)) {
                return STUDENT;
            }
            else {
                return TEACHER;
            }
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean checkLogin(HttpServletRequest request) throws LogicException {
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String password = request.getParameter(PARAM_NAME_PASSWORD);

        UserDAO userDAO = new UserDAO();
        try {
            return userDAO.contains(login, password);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
}
