package by.tanana.courses.logic;

import by.tanana.courses.dao.StudentDAO;
import by.tanana.courses.dao.UserDAO;
import by.tanana.courses.entity.Student;
import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

/**
 * Created by user on 13.04.2015.
 */
public class RegisterLogic {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LASTNAME = "lastname";
    private static final String PARAM_NAME_UNIVERSITY = "university";
    private static final String PARAM_NAME_FACULTY = "faculty";
    private static final String PARAM_NAME_COURSE = "course";
    private static final String PARAM_NAME_GROUP = "group";

    private static final String ATTRIBUTE_NAME_ERROR_LOGIN_EXIST_MESSAGE = "errorLoginExistMessage";

    public static boolean checkRegistrationFields (HttpServletRequest request) {

        MessageManager messageManager = new MessageManager(request);

        boolean check = true;

        if (!Validator.checkLogin(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkPassword(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkName(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkSurname(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkUniversity(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkFaculty(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkCourse(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkGroup(request, messageManager)){
            check = false;
        }
        return check;
    }

    public static boolean checkLogin(HttpServletRequest request) throws LogicException {
        MessageManager messageManager = new MessageManager(request);

        UserDAO userDAO = new UserDAO();
        String login = request.getParameter(PARAM_NAME_LOGIN);
        try {
           if (userDAO.contains(login)) {
               request.setAttribute(ATTRIBUTE_NAME_ERROR_LOGIN_EXIST_MESSAGE, messageManager.getProperty("message.LoginExistError"));
               return false;
           }
            else {
               return true;
           }
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void createUser(HttpServletRequest request) throws LogicException {
        Student student = new Student();
        student.setLogin(request.getParameter(PARAM_NAME_LOGIN));
        student.setPassword(request.getParameter(PARAM_NAME_PASSWORD));
        student.setName(request.getParameter(PARAM_NAME_NAME));
        student.setLastname(request.getParameter(PARAM_NAME_LASTNAME));
        student.setUniversity(request.getParameter(PARAM_NAME_UNIVERSITY));
        student.setFaculty(request.getParameter(PARAM_NAME_FACULTY));
        student.setCourse(Integer.parseInt(request.getParameter(PARAM_NAME_COURSE)));
        student.setGroup(Integer.parseInt(request.getParameter(PARAM_NAME_GROUP)));

        StudentDAO studentDAO = new StudentDAO();
        try {
            studentDAO.createStudent(student);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
}
