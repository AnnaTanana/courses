package by.tanana.courses.logic;

import by.tanana.courses.dao.CourseDAO;
import by.tanana.courses.dao.StudentDAO;
import by.tanana.courses.entity.*;
import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.LogicException;
import by.tanana.courses.resource.MessageManager;
import javafx.util.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 22.04.2015.
 */
public class CourseLogic {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_DESCRIPTION = "description";
    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_COURSE_ID = "courseId";

    public static boolean isDeleted(int id) throws LogicException {
        CourseDAO courseDAO = new CourseDAO();
        try {
            return courseDAO.isDeleted(id);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static Course getCourse(int courseId) throws LogicException {
        CourseDAO courseDAO = new CourseDAO();
        try {
            return courseDAO.getCourseInfo(courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static HashMap<Student, Pair<Integer, Status> > getMarksForCourse(int courseId) throws LogicException {
        CourseDAO courseDAO = new CourseDAO();
        try {
            return courseDAO.marksForCourse(courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static HashMap<Integer, Course> getMoreCourses(String login) throws LogicException {
        CourseDAO courseDAO = new CourseDAO();
        try {
            return courseDAO.getMoreCourses(login);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static HashMap<Integer, Course> getAllCourses() throws LogicException {
        CourseDAO courseDAO = new CourseDAO();
        try {
            return courseDAO.getAllCourses();
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static boolean checkCourseFields (HttpServletRequest request) {
        MessageManager messageManager = new MessageManager(request);

        boolean check = true;

        if (!Validator.checkName(request, messageManager)) {
            check = false;
        }
        if (!Validator.checkDescription(request, messageManager)) {
            check = false;
        }

        return check;
    }

    public static void addCourse(HttpServletRequest request) throws LogicException {
        Course course = new Course();
        course.setName(request.getParameter(PARAM_NAME_NAME));
        String description = request.getParameter(PARAM_NAME_DESCRIPTION);
        description = description.replaceAll("<", "&lt");
        description = description.replaceAll(">", "&gt");
        course.setDescription(description);
        Teacher teacher = new Teacher();
        teacher.setLogin(request.getSession().getAttribute(PARAM_NAME_LOGIN).toString());
        course.setTeacher(teacher);
        course.setHidden(false);
        CourseDAO courseDAO = new CourseDAO();
        try {
            courseDAO.addCourse(course);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void deleteCourse(int courseId) throws LogicException {
        CourseDAO courseDAO = new CourseDAO();
        try {
            courseDAO.deleteCourse(courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }

    public static void updateCourse(HttpServletRequest request) throws LogicException {
        Course course = new Course();
        course.setName(request.getParameter(PARAM_NAME_NAME));
        String description = request.getParameter(PARAM_NAME_DESCRIPTION);
        description = description.replaceAll("<", "&lt");
        description = description.replaceAll(">", "&gt");
        course.setDescription(description);
        int courseId = Integer.parseInt(request.getParameter(PARAM_NAME_COURSE_ID));
        CourseDAO courseDAO = new CourseDAO();
        try {
            courseDAO.updateCourse(course, courseId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
}
