package by.tanana.courses.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * Created by user on 07.05.2015.
 */
@SuppressWarnings("serial")
public class RoleInfoTag extends BodyTagSupport {
    private String role;
    private String login;

    public void setRole(String role) {
        this.role = role;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public int doAfterBody() throws JspException {
        BodyContent content = this.getBodyContent();
        String body = content.getString();
        String res = null;
        if ((login == null || login.isEmpty()) && (role != null && !role.isEmpty())) {
            if (role.equals(pageContext.getSession().getAttribute("role"))) {
                res = body;
                JspWriter out = content.getEnclosingWriter();
                try {
                    out.write(res);
                } catch (IOException e) {
                    throw new JspTagException(e.getMessage());
                }
            }
        } else if ((login != null && !login.isEmpty()) && (role == null || role.isEmpty())) {
            if (login.equals(pageContext.getSession().getAttribute("login"))) {
                res = body;
                JspWriter out = content.getEnclosingWriter();
                try {
                    out.write(res);
                } catch (IOException e) {
                    throw new JspTagException(e.getMessage());
                }
            }
        } else if ((login != null && !login.isEmpty()) && (role != null && !role.isEmpty())) {
            if (role.equals(pageContext.getSession().getAttribute("role")) && login.equals(pageContext.getSession().getAttribute("login"))) {
                res = body;
                JspWriter out = content.getEnclosingWriter();
                try {
                    out.write(res);
                } catch (IOException e) {
                    throw new JspTagException(e.getMessage());
                }
            }
        }
        return SKIP_BODY;
    }
}
