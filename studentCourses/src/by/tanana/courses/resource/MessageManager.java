package by.tanana.courses.resource;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by user on 28.03.2015.
 */
public class MessageManager {
    private static final String SESSION_ATTRIBUTE_LANGUAGE = "language";
    private ResourceBundle resourceBundle;
    // класс извлекает информацию из файла messages.properties
    public MessageManager(HttpServletRequest request) {
        String locale = (String) request.getSession().getAttribute(SESSION_ATTRIBUTE_LANGUAGE);
        if (locale == null) {
            resourceBundle = ResourceBundle.getBundle("resources.messages", new Locale("ru", "ru"));
        }
        else {
            String[] localeArr = locale.split("_");
            resourceBundle = ResourceBundle.getBundle("resources.messages", new Locale(localeArr[0], localeArr[1]));
        }
    }

    public String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
