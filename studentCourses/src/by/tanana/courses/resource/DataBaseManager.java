package by.tanana.courses.resource;

import java.util.ResourceBundle;

/**
 * Created by user on 11.04.2015.
 */
public class DataBaseManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.database");
    // класс извлекает информацию из файла database.properties
    private DataBaseManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
