package by.tanana.courses.dao;

import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.User;
import by.tanana.courses.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10.04.2015.
 */
public class UserDAO extends AbstractDAO {
    private static final String SQL_CHECK_LOGIN = "SELECT login FROM users WHERE login = ?";
    private static final String SQL_CONTAINS_USER = "SELECT login FROM users WHERE login = ? AND password = md5(?);";
    private static final String SQL_DELETE_REVIEW = "DELETE FROM reviews WHERE reviews.reviewId = ?";

    public boolean contains(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CHECK_LOGIN);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while finding user in database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public boolean contains(String login, String password) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CONTAINS_USER);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while finding user in database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void deleteReview(int reviewId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_DELETE_REVIEW);
            statement.setInt(1, reviewId);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while deleting a review.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }
}
