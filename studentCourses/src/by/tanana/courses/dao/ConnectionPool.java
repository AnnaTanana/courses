package by.tanana.courses.dao;

import by.tanana.courses.exception.PoolException;
import by.tanana.courses.resource.DataBaseManager;
import org.apache.log4j.Logger;

import java.nio.charset.CoderMalfunctionError;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by user on 29.03.2015.
 */
public final class  ConnectionPool {
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);

    private static ConnectionPool instance;

    private static final ReentrantLock LOCK = new ReentrantLock();

    private static final int POOL_SIZE = 10;
    private static final int TIME_WAIT = 1000;

    private static AtomicBoolean hasInstance = new AtomicBoolean(false);
    private static AtomicBoolean available = new AtomicBoolean(false);

    private BlockingQueue<ConnectionWrapper> freeConnections;
    private BlockingQueue<ConnectionWrapper> workingConnections;

    private static String url;
    private static String user;
    private static String password;

    static {
        url = DataBaseManager.getProperty("url");
        user = DataBaseManager.getProperty("user");
        password = DataBaseManager.getProperty("password");
    }

    private ConnectionPool() {}

    public static ConnectionPool getInstance() {
        if (!hasInstance.get()) {
            try {
                if (LOCK.tryLock(TIME_WAIT, TimeUnit.MILLISECONDS)) {
                    if (instance == null) {
                        instance = new ConnectionPool();
                        hasInstance.set(true);
                    }
                    LOCK.unlock();
                }
            } catch (InterruptedException e) {
                LOG.error(e);
            } 
        }
        return instance;
    }

    public void init() throws PoolException {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            freeConnections = new ArrayBlockingQueue<ConnectionWrapper>(10);
            workingConnections = new ArrayBlockingQueue<ConnectionWrapper>(10);
            for (int i = 0; i < POOL_SIZE; i++) {
                ConnectionWrapper connection = new ConnectionWrapper(DriverManager.getConnection(url, user, password));
                freeConnections.add(connection);
            }
            available.set(true);
        } catch (SQLException e) {
            throw new PoolException("Error while initializing pool", e);
        }
    }

    public ConnectionWrapper takeConnection(int waitTime) throws PoolException {
        ConnectionWrapper connection = null;
        try {
            if (available.get()) {
                connection = freeConnections.poll(waitTime, TimeUnit.SECONDS);
                if (connection != null)
                {
                    workingConnections.add(connection);
                    return connection;
                }
                else {
                    throw new PoolException("Connection timeout.");
                }
            }
            else {
                throw new PoolException("Pool is not initialized yet.");
            }
        } catch (InterruptedException e) {
            throw new PoolException(e);
        }
    }

    public void releaseConnection(ConnectionWrapper connection) {
        try {
            workingConnections.remove(connection);
            freeConnections.put(connection);
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    private void clearConnectionQueue() throws SQLException {
        ConnectionWrapper connection;
        while ((connection = freeConnections.poll()) != null) {
            connection.closeConnection();
        }
        while ((connection = workingConnections.poll()) != null) {
            connection.closeConnection();
        }

    }

    public void closePool() {
        try {
            available.set(false);
            Thread.sleep(TIME_WAIT);
            if (instance != null) {
                instance.clearConnectionQueue();
                instance = null;
            }
        } catch (SQLException | InterruptedException e) {
            LOG.error("Unable to close connection pool");
        }
    }
}
