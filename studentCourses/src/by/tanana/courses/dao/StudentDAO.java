package by.tanana.courses.dao;

import by.tanana.courses.entity.*;
import by.tanana.courses.exception.DAOException;
import javafx.util.Pair;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 28.03.2015.
 */
public class StudentDAO extends AbstractDAO {
    private static final String SQL_CREATE_USER = "INSERT INTO users (login, password) VALUES (?, md5(?));";
    private static final String SQL_CREATE_STUDENT = "INSERT INTO students (userId, name, lastname, university, faculty, course, groupNum) VALUES (LAST_INSERT_ID(), ?, ?, ?, ?, ?, ?);";
    private static final String SQL_CHECK_STUDENT = "SELECT users.login FROM users JOIN students ON users.userId = students.userId WHERE users.login = ?";
    private static final String SQL_GET_STUDENT_ID = "SELECT students.studentId FROM students JOIN users ON users.userId = students.userId WHERE users.login = ?";
    private static final String SQL_GET_STUDENTS_COURSES = "SELECT courses.courseId,courses.name, marks.mark, marks.status, courses.hidden FROM users JOIN students ON users.userId = students.userId JOIN marks ON marks.studentId = students.studentId JOIN courses ON courses.courseId = marks.courseId WHERE users.login = ?";
    private static final String SQL_CANCEL_COURSE = "UPDATE marks SET marks.status = 'cancelled' WHERE marks.studentId = ? AND marks.courseId = ?";
    private static final String SQL_GET_STUDENT_INFO = "SELECT students.name, students.lastname, students.university, students.faculty, students.course, students.groupNum FROM students JOIN users ON students.userId = users.userId WHERE users.login = ?";
    private static final String SQL_REGISTER_FOR_A_COURSE = "INSERT INTO marks (studentId, courseId, mark, status) VALUES (?, ?, 0, 'attending')";
    private static final String SQL_SELECT_ALL_STUDENTS = "SELECT name, lastname, login FROM students JOIN users ON users.userId = students.userId";
    private static final String SQL_BAN = "INSERT INTO blacklist (login) VALUES (?)";
    private static final String SQL_CHECK_STUDENT_IN_BLACKLIST = "SELECT blacklist.login FROM blacklist JOIN users ON users.login = blacklist.login WHERE users.login = ?";
    private static final String SQL_DELETE_FROM_BLACKLIST = "DELETE FROM blacklist WHERE blacklist.login = ?";
    private static final String SQL_SELECT_BLACKLIST = "SELECT blacklist.login, students.name, students.lastname FROM blacklist JOIN users ON users.login = blacklist.login JOIN students ON students.userId = users.userId";
    private static final String SQL_ADD_REVIEW = "INSERT INTO reviews (studentId, courseId, review) VALUES(?, ?, ?)";
    private static final String SQL_CHECK_ATTENDING_COURSE = "SELECT marks.mark FROM marks JOIN students ON students.studentId = marks.studentId JOIN users ON users.userId = students.userId WHERE users.login = ? AND marks.courseId = ?";
    private static final String SQL_UPDATE_STUDENT = "UPDATE students SET students.name = ?, students.lastname = ?, students.university = ?, students.faculty = ?, students.course = ?, students.groupNum = ? WHERE students.studentId = ?";
    private static final String SQL_CHANGE_MARK = "UPDATE marks SET marks.mark = ? WHERE marks.studentId = ? AND marks.courseId = ?";
    private static final String SQL_CHANGE_STATUS = "UPDATE marks SET marks.status = ? WHERE marks.studentId = ? AND marks.courseId = ?";

    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LASTNAME = "lastname";
    private static final String PARAM_NAME_COURSE_ID = "courseId";
    private static final String PARAM_NAME_MARK = "mark";
    private static final String PARAM_NAME_STUDENT_ID = "studentId";
    private static final String PARAM_NAME_UNIVERSITY = "university";
    private static final String PARAM_NAME_FACULTY = "faculty";
    private static final String PARAM_NAME_COURSE = "course";
    private static final String PARAM_NAME_GROUP_NUMBER = "groupNum";
    private static final String PARAM_NAME_STATUS = "marks.status";
    private static final String PARAM_NAME_HIDDEN = "hidden";

    public void createStudent(Student student) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CREATE_USER);
            statement.setString(1, student.getLogin());
            statement.setString(2, student.getPassword());
            statement.executeUpdate();

            statement.clearParameters();

            statement = connection.getStatement(SQL_CREATE_STUDENT);

            statement.setString(1, student.getName());
            statement.setString(2, student.getLastname());
            statement.setString(3, student.getUniversity());
            statement.setString(4, student.getFaculty());
            statement.setInt(5, student.getCourse());
            statement.setInt(6, student.getGroup());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while creating a student.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public boolean contains(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CHECK_STUDENT);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while finding user in database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public HashMap<Pair<Integer, String>, Pair<Integer, Status>> getCourses(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        HashMap<Pair<Integer, String>, Pair<Integer, Status>> coursesAndMarks = new HashMap<Pair<Integer, String>, Pair<Integer, Status>>();
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_STUDENTS_COURSES);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int courseId = resultSet.getInt(PARAM_NAME_COURSE_ID);
                String course = resultSet.getString(PARAM_NAME_NAME);
                int mark = resultSet.getInt(PARAM_NAME_MARK);
                String hidden = resultSet.getString(PARAM_NAME_HIDDEN);
                Status status;
                if ("true".equals(hidden)) {
                    status = Status.DELETED;
                } else {
                    status = Status.valueOf(resultSet.getString(PARAM_NAME_STATUS).toUpperCase());
                }
                coursesAndMarks.put(new Pair<Integer, String>(courseId, course), new Pair<Integer, Status>(mark, status));
            }
            return coursesAndMarks;
        } catch (SQLException e) {
            throw new DAOException("Error while getting students courses and marks from database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void cancelCourse(String login, int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();

            statement = connection.getStatement(SQL_GET_STUDENT_ID);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int studentId = resultSet.getInt(PARAM_NAME_STUDENT_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_CANCEL_COURSE);
            statement.setInt(1, studentId);
            statement.setInt(2, courseId);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while canceling a course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }

    }

    public Student getStudentInfo(String login) throws DAOException {
        ConnectionWrapper connection = null;
        Student student = new Student();
        PreparedStatement st = null;
        try {
            connection = getConnection();
            st = connection.getStatement(SQL_GET_STUDENT_INFO);
            st.setString(1, login);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                student.setName(resultSet.getString(PARAM_NAME_NAME));
                student.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                student.setUniversity(resultSet.getString(PARAM_NAME_UNIVERSITY));
                student.setFaculty(resultSet.getString(PARAM_NAME_FACULTY));
                student.setCourse(resultSet.getInt(PARAM_NAME_COURSE));
                student.setGroup(resultSet.getInt(PARAM_NAME_GROUP_NUMBER));
            }
            return student;
        } catch (SQLException e) {
            throw new DAOException("Error while finding a student.", e);
        } finally {
            connection.closeStatement(st);
            returnConnection(connection);
        }
    }

    public void takeCourse(String login, int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_STUDENT_ID);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int studentId = resultSet.getInt(PARAM_NAME_STUDENT_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_REGISTER_FOR_A_COURSE);

            statement.setInt(1, studentId);
            statement.setInt(2, courseId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while register for a course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public ArrayList<Student> getAllStudents() throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        ArrayList<Student> students = new ArrayList<>();
        try{
            connection = getConnection();
            statement = connection.getStatement(SQL_SELECT_ALL_STUDENTS);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Student student = new Student();
                student.setName(resultSet.getString(PARAM_NAME_NAME));
                student.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                student.setLogin(resultSet.getString(PARAM_NAME_LOGIN));
                students.add(student);
            }
            return students;
        } catch (SQLException e) {
            throw new DAOException("Error while getting students.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void ban(String login) throws DAOException{
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();

            statement = connection.getStatement(SQL_BAN);
            statement.setString(1, login);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while baning a student.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public boolean isInBlackList(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CHECK_STUDENT_IN_BLACKLIST);
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while finding user in database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void deleteFromBlackList(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_DELETE_FROM_BLACKLIST);
            statement.setString(1, login);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while finding user in blacklist.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public ArrayList<Student> showBlackList() throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement st = null;
        ArrayList<Student> students = new ArrayList<>();
        try {
            connection = getConnection();
            st = connection.getStatement(SQL_SELECT_BLACKLIST);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Student student = new Student();
                student.setLogin(resultSet.getString(PARAM_NAME_LOGIN));
                student.setName(resultSet.getString(PARAM_NAME_NAME));
                student.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                students.add(student);
            }
            return students;
        } catch (SQLException e) {
            throw new DAOException("Error while finding a blacklist.", e);
        } finally {
            connection.closeStatement(st);
            returnConnection(connection);
        }
    }

    public void addReview(Review review, int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();

            statement = connection.getStatement(SQL_GET_STUDENT_ID);
            statement.setString(1, review.getStudent().getLogin());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int studentId = resultSet.getInt(PARAM_NAME_STUDENT_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_ADD_REVIEW);

            statement.setInt(1, studentId);
            statement.setInt(2, courseId);
            statement.setString(3, review.getReview());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while adding a review.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public boolean isAttendingCourse(String login, int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CHECK_ATTENDING_COURSE);
            statement.setString(1, login);
            statement.setInt(2, courseId);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while checking if user's attending the course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void updateStudent(Student student) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_STUDENT_ID);
            statement.setString(1, student.getLogin());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int studentId = resultSet.getInt(PARAM_NAME_STUDENT_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_UPDATE_STUDENT);

            statement.setString(1, student.getName());
            statement.setString(2, student.getLastname());
            statement.setString(3, student.getUniversity());
            statement.setString(4, student.getFaculty());
            statement.setInt(5, student.getCourse());
            statement.setInt(6, student.getGroup());
            statement.setInt(7, studentId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while updating a student.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void changeMark(String login, int mark, int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_STUDENT_ID);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int studentId = resultSet.getInt(PARAM_NAME_STUDENT_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_CHANGE_MARK);

            statement.setInt(1, mark);
            statement.setInt(2, studentId);
            statement.setInt(3, courseId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while changing mark.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void changeStatus(String login, Status status, int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_STUDENT_ID);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int studentId = resultSet.getInt(PARAM_NAME_STUDENT_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_CHANGE_STATUS);

            statement.setString(1, status.toString().toLowerCase());
            statement.setInt(2, studentId);
            statement.setInt(3, courseId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while changing status.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }
}
