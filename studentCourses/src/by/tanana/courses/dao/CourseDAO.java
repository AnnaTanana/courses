package by.tanana.courses.dao;

import by.tanana.courses.entity.*;
import by.tanana.courses.exception.DAOException;
import javafx.util.Pair;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 21.04.2015.
 */
public class CourseDAO extends AbstractDAO {
    private static final String SQL_SELECT_COURSE_INFO = "SELECT courses.name, courses.description FROM courses WHERE courses.courseId = ?";
    private static final String SQL_SELECT_TEACHER = "SELECT lecturers.name, lecturers.lastname, users.login FROM courses JOIN lecturers ON courses.lecturerId = lecturers.lecturerId JOIN users ON lecturers.userId = users.userId WHERE courses.courseId = ?";
    private static final String SQL_SELECT_STUDENTS = "SELECT students.name, students.lastname, users.login, marks.mark, marks.status FROM courses JOIN marks ON marks.courseId = courses.courseId JOIN students ON students.studentId = marks.studentId JOIN users ON users.userId = students.userId WHERE courses.courseId = ? AND marks.status <> 'cancelled'";
    private static final String SQL_SELECT_REVIEWS = "SELECT reviews.reviewId, reviews.review, users.login, students.name, students.lastname  FROM reviews JOIN courses ON courses.courseId = reviews.courseId JOIN students ON students.studentId = reviews.studentId JOIN users ON users.userId = students.userId WHERE courses.courseId = ?";
    private static final String SQL_SELECT_COURSES = "SELECT  courses.courseId, courses.name, users.login, lecturers.name, lecturers.lastname FROM courses JOIN lecturers ON lecturers.lecturerId = courses.lecturerId JOIN users ON users.userId = lecturers.userId WHERE courses.courseId NOT IN (SELECT marks.courseId FROM marks JOIN students ON students.studentId = marks.studentId JOIN users ON users.userId = students.userId WHERE users.login = ?) AND courses.hidden = 'false'";
    private static final String SQL_SELECT_ALL_COURSES = "SELECT  courses.courseId, courses.name, users.login, lecturers.name, lecturers.lastname FROM courses JOIN lecturers ON lecturers.lecturerId = courses.lecturerId JOIN users ON users.userId = lecturers.userId WHERE courses.hidden = 'false' ";
    private static final String SQL_GET_TEACHER_ID = "SELECT lecturers.lecturerId FROM lecturers JOIN users ON users.userId = lecturers.userId WHERE users.login = ?";
    private static final String SQL_CREATE_COURSE = "INSERT INTO courses (name, lecturerId, description, hidden) VALUES (?, ?, ?, ?)";
    private static final String SQL_DELETE_COURSE = "UPDATE courses SET courses.hidden='true' WHERE courses.courseId = ?";
    private static final String SQL_UPDATE_COURSE = "UPDATE courses SET courses.name = ?, courses.description = ? WHERE courses.courseId = ?";
    private static final String SQL_CHECK_HIDDEN = "SELECT courses.hidden FROM courses WHERE courses.courseId = ?";

    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_USER_LOGIN = "users.login";
    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LASTNAME = "lastname";
    private static final String PARAM_NAME_LECTURERS_NAME = "lecturers.name";
    private static final String PARAM_NAME_LECTURERS_LASTNAME = "lecturers.lastname";
    private static final String PARAM_NAME_COURSE_ID = "courses.courseId";
    private static final String PARAM_NAME_COURSE_NAME = "courses.name";
    private static final String PARAM_NAME_DESCRIPTION = "description";
    private static final String PARAM_NAME_REVIEW = "review";
    private static final String PARAM_NAME_REVIEW_ID = "reviewId";
    private static final String PARAM_NAME_MARK = "mark";
    private static final String PARAM_NAME_STATUS = "status";
    private static final String PARAM_NAME_HIDDEN = "hidden";

    /**
     * Checking if course was deleted by lecturer
     * @param id of the course
     * @return true if course was deleted,
     * false if it wasn't
     * @throws DAOException
     */
    public boolean isDeleted(int id) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_CHECK_HIDDEN);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            String hidden = rs.getString(PARAM_NAME_HIDDEN);
            if ("true".equals(hidden)) {
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while finding course in database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    /**
     * Getting student's marks and status for
     * the following course for the students
     * attending this course
     * @param courseId of the following course
     * @return HashMap where Student is a key
     * and value is a pair of student's mark and status
     * @throws DAOException
     */
    public HashMap<Student, Pair<Integer, Status> > marksForCourse(int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        HashMap<Student, Pair<Integer, Status> > marksForCourse = new HashMap<>();
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_SELECT_STUDENTS);
            statement.setInt(1, courseId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Student student = new Student();
                student.setName(resultSet.getString(PARAM_NAME_NAME));
                student.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                student.setLogin(resultSet.getString(PARAM_NAME_LOGIN));
                int mark = resultSet.getInt(PARAM_NAME_MARK);
                Status status = Status.valueOf(resultSet.getString(PARAM_NAME_STATUS).toUpperCase());
                marksForCourse.put(student, new Pair<Integer, Status>(mark, status));
            }
            return marksForCourse;
        } catch (SQLException e) {
            throw new DAOException("Error while finding student info.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }

    }

    /**
     * Getting course info
     * @param courseId of the following course
     * @return a Course instance
     * @throws DAOException
     */
    public Course getCourseInfo(int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        Course course = new Course();
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_SELECT_COURSE_INFO);
            statement.setInt(1, courseId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString(PARAM_NAME_NAME);
                String description = resultSet.getString(PARAM_NAME_DESCRIPTION);
                course.setName(name);
                course.setDescription(description);
            }

            statement.clearParameters();
            statement = connection.getStatement(SQL_SELECT_TEACHER);
            statement.setInt(1, courseId);
            resultSet = statement.executeQuery();
            Teacher teacher = new Teacher();
            while (resultSet.next()) {
                teacher.setName(resultSet.getString(PARAM_NAME_NAME));
                teacher.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                teacher.setLogin(resultSet.getString(PARAM_NAME_LOGIN));
            }
            course.setTeacher(teacher);

            statement.clearParameters();
            statement = connection.getStatement(SQL_SELECT_STUDENTS);
            statement.setInt(1, courseId);
            resultSet = statement.executeQuery();
            ArrayList<Student> students = new ArrayList<Student>();
            while (resultSet.next()) {
                Student student = new Student();
                student.setName(resultSet.getString(PARAM_NAME_NAME));
                student.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                student.setLogin(resultSet.getString(PARAM_NAME_LOGIN));
                students.add(student);
            }
            course.setStudents(students);

            statement.clearParameters();
            statement = connection.getStatement(SQL_SELECT_REVIEWS);
            statement.setInt(1, courseId);
            resultSet = statement.executeQuery();
            ArrayList<Review> reviews = new ArrayList<Review>();
            while (resultSet.next()) {
                Review review = new Review();
                review.setReview(resultSet.getString(PARAM_NAME_REVIEW));
                review.setReviewId(resultSet.getInt(PARAM_NAME_REVIEW_ID));
                Student student = new Student();
                student.setLogin(resultSet.getString(PARAM_NAME_LOGIN));
                student.setName(resultSet.getString(PARAM_NAME_NAME));
                student.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                review.setStudent(student);
                reviews.add(review);
            }
            course.setReviews(reviews);

            return course;
        } catch (SQLException e) {
            throw new DAOException("Error while finding a course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    /**
     * Getting courses which the following student
     * hasn't chosen already
     * @param login of the student
     * @return HashMap where value is course id and
     * value is a Course instance
     * @throws DAOException
     */
    public HashMap<Integer, Course> getMoreCourses(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        HashMap<Integer, Course> courses = new HashMap<>();
        try{
            connection = getConnection();
            statement = connection.getStatement(SQL_SELECT_COURSES);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Course course = new Course();
                Teacher teacher = new Teacher();
                teacher.setName(resultSet.getString(PARAM_NAME_LECTURERS_NAME));
                teacher.setLastname(resultSet.getString(PARAM_NAME_LECTURERS_LASTNAME));
                teacher.setLogin(resultSet.getString(PARAM_NAME_USER_LOGIN));
                course.setTeacher(teacher);
                course.setName(resultSet.getString(PARAM_NAME_COURSE_NAME));
                int courseId = resultSet.getInt(PARAM_NAME_COURSE_ID);
                courses.put(courseId, course);
            }
        return courses;
        } catch (SQLException e) {
            throw new DAOException("Error while getting courses.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    /**
     * Getting all courses from database
     * @return HashMap where the key is course id and
     * the value is a Course instance
     * @throws DAOException
     */
    public HashMap<Integer, Course> getAllCourses() throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        HashMap<Integer, Course> courses = new HashMap<>();
        try{
            connection = getConnection();
            statement = connection.getStatement(SQL_SELECT_ALL_COURSES);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Course course = new Course();
                Teacher teacher = new Teacher();
                teacher.setName(resultSet.getString(PARAM_NAME_LECTURERS_NAME));
                teacher.setLastname(resultSet.getString(PARAM_NAME_LECTURERS_LASTNAME));
                teacher.setLogin(resultSet.getString(PARAM_NAME_USER_LOGIN));
                course.setTeacher(teacher);
                course.setName(resultSet.getString(PARAM_NAME_COURSE_NAME));
                int courseId = resultSet.getInt(PARAM_NAME_COURSE_ID);
                courses.put(courseId, course);
            }
            return courses;
        } catch (SQLException e) {
            throw new DAOException("Error while getting courses.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    /**
     * Adding course by teacher
     * @param course which is going to be added
     * @throws DAOException
     */
    public void addCourse(Course course) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_TEACHER_ID);
            statement.setString(1, course.getTeacher().getLogin());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int teacherId = resultSet.getInt("lecturerId");

            statement.clearParameters();

            statement = connection.getStatement(SQL_CREATE_COURSE);

            statement.setString(1, course.getName());
            statement.setInt(2, teacherId);
            statement.setString(3, course.getDescription());
            statement.setString(4, course.isHidden().toString());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while creating a course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    /**
     * Deleting course by it's id
     * @param courseId
     * @throws DAOException
     */
    public void deleteCourse(int courseId) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();

            statement = connection.getStatement(SQL_DELETE_COURSE);
            statement.setInt(1, courseId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while deleting a course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    /**
     * Editing fields of the existing course
     * @param course
     * @param courseId
     * @throws DAOException
     */
    public void updateCourse(Course course, int courseId) throws DAOException{
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_UPDATE_COURSE);

            statement.setString(1, course.getName());
            statement.setString(2, course.getDescription());
            statement.setInt(3, courseId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while updating a course.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }
}

