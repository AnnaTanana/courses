package by.tanana.courses.dao;

import by.tanana.courses.exception.DAOException;
import by.tanana.courses.exception.PoolException;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

/**
 * Created by user on 28.03.2015.
 */
public abstract class AbstractDAO {
    private static final int WAIT_TIME_SEC = 10;

    public ConnectionWrapper getConnection() throws DAOException {
        try {
            return ConnectionPool.getInstance().takeConnection(WAIT_TIME_SEC);
        } catch (PoolException e) {
            throw new DAOException(e);
        }
    }

    public void returnConnection(ConnectionWrapper connection) throws DAOException {
        ConnectionPool.getInstance().releaseConnection(connection);
    }
}
