package by.tanana.courses.dao;

import by.tanana.courses.entity.Student;
import by.tanana.courses.entity.Teacher;
import by.tanana.courses.exception.DAOException;
import javafx.util.Pair;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by user on 27.04.2015.
 */
public class TeacherDAO extends AbstractDAO {

    private static final String SQL_GET_TEACHER_INFO = "SELECT lecturers.name, lecturers.lastname, lecturers.university, lecturers.position FROM lecturers JOIN users ON lecturers.userId = users.userId WHERE users.login = ?";
    private static final String SQL_GET_TEACHER_COURSES = "SELECT courses.courseId, courses.name FROM courses JOIN lecturers ON courses.lecturerId = lecturers.lecturerId JOIN users ON lecturers.userId = users.userId WHERE users.login = ? AND courses.hidden = 'false'";
    private static final String SQL_GET_TEACHER_ID = "SELECT lecturers.lecturerId FROM lecturers JOIN users ON users.userId = lecturers.userId WHERE users.login = ?";
    private static final String SQL_UPDATE_TEACHER = "UPDATE lecturers SET lecturers.name = ?, lecturers.lastname = ?, lecturers.university = ?, lecturers.position = ? WHERE lecturers.lecturerId = ?";


    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LASTNAME = "lastname";
    private static final String PARAM_NAME_UNIVERSITY = "university";
    private static final String PARAM_NAME_POSITION = "position";
    private static final String PARAM_NAME_COURSE_ID = "courseId";
    private static final String PARAM_NAME_TEACHER_ID = "lecturerId";

    public Teacher getTeacherInfo(String login) throws DAOException {
        ConnectionWrapper connection = null;
        Teacher teacher = new Teacher();
        PreparedStatement st = null;
        try {
            connection = getConnection();
            st = connection.getStatement(SQL_GET_TEACHER_INFO);
            st.setString(1, login);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                teacher.setName(resultSet.getString(PARAM_NAME_NAME));
                teacher.setLastname(resultSet.getString(PARAM_NAME_LASTNAME));
                teacher.setUniversity(resultSet.getString(PARAM_NAME_UNIVERSITY));
                teacher.setPosition(resultSet.getString(PARAM_NAME_POSITION));

            }
            return teacher;
        } catch (SQLException e) {
            throw new DAOException("Error while finding a teacher.", e);
        } finally {
            connection.closeStatement(st);
            returnConnection(connection);
        }
    }

    public HashMap<Integer, String> getCourses(String login) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;
        HashMap<Integer, String> coursesAndMarks = new HashMap<Integer, String>();
        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_TEACHER_COURSES);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int courseId = resultSet.getInt(PARAM_NAME_COURSE_ID);
                String course = resultSet.getString(PARAM_NAME_NAME);
                coursesAndMarks.put(courseId, course);
            }
            return coursesAndMarks;
        } catch (SQLException e) {
            throw new DAOException("Error while getting students courses and marks from database.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }

    public void updateTeacher(Teacher teacher) throws DAOException {
        ConnectionWrapper connection = null;
        PreparedStatement statement = null;

        try {
            connection = getConnection();
            statement = connection.getStatement(SQL_GET_TEACHER_ID);
            statement.setString(1, teacher.getLogin());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int teacherId = resultSet.getInt(PARAM_NAME_TEACHER_ID);

            statement.clearParameters();

            statement = connection.getStatement(SQL_UPDATE_TEACHER);

            statement.setString(1, teacher.getName());
            statement.setString(2, teacher.getLastname());
            statement.setString(3, teacher.getUniversity());
            statement.setString(4, teacher.getPosition());
            statement.setInt(5, teacherId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Error while updating a teacher.", e);
        } finally {
            connection.closeStatement(statement);
            returnConnection(connection);
        }
    }
}
